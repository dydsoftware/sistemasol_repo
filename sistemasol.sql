/*
 Navicat Premium Data Transfer

 Source Server         : 0_localhost_php7
 Source Server Type    : MySQL
 Source Server Version : 100406
 Source Host           : localhost:3308
 Source Schema         : sistemasol

 Target Server Type    : MySQL
 Target Server Version : 100406
 File Encoding         : 65001

 Date: 28/11/2020 18:49:42
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for bcompralote
-- ----------------------------
DROP TABLE IF EXISTS `bcompralote`;
CREATE TABLE `bcompralote`  (
  `idb` int(11) NOT NULL AUTO_INCREMENT,
  `bclienteid` int(11) NOT NULL,
  `idloteb` int(11) NOT NULL,
  `bmoneda` varchar(3) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `plan` int(2) NOT NULL,
  `adelanto` decimal(15, 2) NOT NULL,
  `mtotal` decimal(15, 2) NOT NULL,
  `vcuota` decimal(15, 2) NOT NULL,
  `vcuotapesos` decimal(15, 2) NOT NULL,
  `cambio` decimal(15, 2) NOT NULL,
  `bfechaalta` date NOT NULL,
  `bfechavenceplan` date NOT NULL,
  PRIMARY KEY (`idb`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bcompralote
-- ----------------------------
INSERT INTO `bcompralote` VALUES (1, 1, 1, 'U$S', 18, 1000.00, 6000.00, 277.77, 19166.67, 0.00, '2020-09-17', '2022-03-17');
INSERT INTO `bcompralote` VALUES (17, 4, 126, 'U$S', 18, 2500.00, 4500.00, 250.00, 34500.00, 138.00, '2020-11-17', '2020-11-17');
INSERT INTO `bcompralote` VALUES (18, 2, 10, 'U$S', 18, 2500.00, 4000.00, 222.22, 30666.67, 138.00, '2020-11-17', '2020-11-17');

-- ----------------------------
-- Table structure for cliente
-- ----------------------------
DROP TABLE IF EXISTS `cliente`;
CREATE TABLE `cliente`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nya` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `pass` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `avatar` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `telefono` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `fecha` date NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cliente
-- ----------------------------
INSERT INTO `cliente` VALUES (1, 'Mario Perez', 'marioperez@email.com', 'mario1234', 'img/img9.jpg', '1100003322', '2020-09-01');
INSERT INTO `cliente` VALUES (2, 'Luis Puerta', 'luis@mail.com', 'luis1234', 'img/img9.jpg', '1133558844', '2020-10-28');
INSERT INTO `cliente` VALUES (3, 'Marcelo Cossa', 'marcelo@mail.com', 'tresde0599', 'img/img9.jpg', '1130305333', '2020-11-02');
INSERT INTO `cliente` VALUES (4, 'Ana Rojas', 'ana@mail.com', 'tresde0599', 'img/img9.jpg', '1130305333', '2020-11-10');
INSERT INTO `cliente` VALUES (5, 'Magali Perez', 'magali@mail.com', 'tresde0599', 'img/img9.jpg', '1130305333', '2020-11-11');
INSERT INTO `cliente` VALUES (6, 'cliente 1', 'alexistomaselli@gmail.com', '1234', 'img/avatar.jpg', '', '2020-11-20');
INSERT INTO `cliente` VALUES (7, 'cliente 1', 'alexistomaselli@gmail.com', '1234', 'img/avatar.jpg', '', '2020-11-20');

-- ----------------------------
-- Table structure for fclote
-- ----------------------------
DROP TABLE IF EXISTS `fclote`;
CREATE TABLE `fclote`  (
  `idf` int(11) NOT NULL AUTO_INCREMENT,
  `clientid` int(11) NOT NULL,
  `prefix` varchar(4) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `lcuota` int(11) NOT NULL,
  `fnumero` int(11) NOT NULL,
  `dess` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `subtotal` decimal(15, 2) NOT NULL,
  `total_imp` decimal(15, 2) NOT NULL,
  `total` decimal(15, 2) NOT NULL,
  `ftipopago` int(11) NOT NULL,
  `festado` int(11) NOT NULL,
  `fechaalta` date NOT NULL,
  `fechavence` date NOT NULL,
  PRIMARY KEY (`idf`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fclote
-- ----------------------------
INSERT INTO `fclote` VALUES (1, 1, 'FAC-', 1, 1, 'Pago de Lote #6', 277.77, 0.00, 277.77, 1, 1, '2020-09-28', '2020-10-10');
INSERT INTO `fclote` VALUES (2, 1, 'FAC-', 1, 2, 'Pago de Lote #6', 166.66, 0.00, 166.66, 1, 1, '2020-10-28', '2020-11-10');

-- ----------------------------
-- Table structure for fexpensas
-- ----------------------------
DROP TABLE IF EXISTS `fexpensas`;
CREATE TABLE `fexpensas`  (
  `eid` int(11) NOT NULL AUTO_INCREMENT,
  `clientid` int(11) NOT NULL,
  `prefx` varchar(4) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `fnumero` int(11) NOT NULL,
  `eperiodo` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `dess` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `subtotal` decimal(15, 2) NOT NULL,
  `total_imp` decimal(15, 2) NOT NULL,
  `total` decimal(15, 2) NOT NULL,
  `etipo` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `eestado` int(11) NOT NULL,
  `efechaalta` date NOT NULL,
  `efechavence` date NOT NULL,
  PRIMARY KEY (`eid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fexpensas
-- ----------------------------
INSERT INTO `fexpensas` VALUES (1, 1, 'FAC-', 1, 'septiembre', 'Lorem ipsum dolor sit, amet consectetur adipisicing elit.<br>\r\nLibero quod debitis repellat possimus ipsum officiis magni repellendus, sunt qui cumque ea odit sequi voluptatum doloribus?<br>\r\n    Adipisci odio velit officiis laudantium.', 5000.00, 0.00, 5000.00, 'transferencia', 1, '2020-08-28', '2020-09-10');
INSERT INTO `fexpensas` VALUES (2, 1, 'FAC-', 4, 'octubre', 'Lorem ipsum dolor sit, amet consectetur adipisicing elit.<br>\r\nLibero quod debitis repellat possimus ipsum officiis magni repellendus, sunt qui cumque ea odit sequi voluptatum doloribus?<br>\r\n    Adipisci odio velit officiis laudantium.', 5000.00, 0.00, 5000.00, 'transferencia', 0, '2020-09-28', '2020-10-10');

-- ----------------------------
-- Table structure for infolote
-- ----------------------------
DROP TABLE IF EXISTS `infolote`;
CREATE TABLE `infolote`  (
  `idlote` int(11) NOT NULL AUTO_INCREMENT,
  `lotenumero` int(11) NOT NULL,
  `manzana` int(3) NOT NULL,
  `mz` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `barrio` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `loteestado` int(1) NOT NULL,
  PRIMARY KEY (`idlote`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 135 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of infolote
-- ----------------------------
INSERT INTO `infolote` VALUES (2, 2, 83, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (3, 3, 83, '', 'Sol de Domselaar', 1);
INSERT INTO `infolote` VALUES (5, 5, 83, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (6, 6, 83, '', 'Sol de Domselaar', 1);
INSERT INTO `infolote` VALUES (7, 7, 83, '', 'Sol de Domselaar', 1);
INSERT INTO `infolote` VALUES (8, 8, 83, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (9, 9, 83, '', 'Sol de Domselaar', 1);
INSERT INTO `infolote` VALUES (13, 13, 83, '', 'Sol de Domselaar', 1);
INSERT INTO `infolote` VALUES (14, 14, 83, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (15, 15, 83, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (16, 16, 83, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (17, 17, 83, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (18, 18, 83, '', 'Sol de Domselaar', 1);
INSERT INTO `infolote` VALUES (19, 19, 83, '', 'Sol de Domselaar', 1);
INSERT INTO `infolote` VALUES (20, 20, 83, '', 'Sol de Domselaar', 1);
INSERT INTO `infolote` VALUES (21, 21, 83, '', 'Sol de Domselaar', 1);
INSERT INTO `infolote` VALUES (22, 22, 83, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (23, 23, 83, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (24, 24, 83, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (25, 25, 83, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (26, 26, 83, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (27, 27, 83, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (28, 28, 83, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (29, 29, 83, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (30, 30, 83, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (31, 31, 83, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (32, 32, 83, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (33, 1, 84, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (34, 2, 84, '', 'Sol de Domselaar', 1);
INSERT INTO `infolote` VALUES (35, 3, 84, '', 'Sol de Domselaar', 1);
INSERT INTO `infolote` VALUES (36, 4, 84, '', 'Sol de Domselaar', 1);
INSERT INTO `infolote` VALUES (37, 20, 84, '', 'Sol de Domselaar', 1);
INSERT INTO `infolote` VALUES (38, 21, 84, '', 'Sol de Domselaar', 1);
INSERT INTO `infolote` VALUES (39, 22, 84, '', 'Sol de Domselaar', 1);
INSERT INTO `infolote` VALUES (40, 23, 84, '', 'Sol de Domselaar', 1);
INSERT INTO `infolote` VALUES (41, 24, 84, '', 'Sol de Domselaar', 1);
INSERT INTO `infolote` VALUES (42, 25, 84, '', 'Sol de Domselaar', 1);
INSERT INTO `infolote` VALUES (43, 26, 84, '', 'Sol de Domselaar', 1);
INSERT INTO `infolote` VALUES (44, 27, 84, '', 'Sol de Domselaar', 1);
INSERT INTO `infolote` VALUES (45, 28, 84, '', 'Sol de Domselaar', 1);
INSERT INTO `infolote` VALUES (46, 29, 84, '', 'Sol de Domselaar', 1);
INSERT INTO `infolote` VALUES (47, 30, 84, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (48, 31, 84, '', 'Sol de Domselaar', 1);
INSERT INTO `infolote` VALUES (49, 32, 84, '', 'Sol de Domselaar', 1);
INSERT INTO `infolote` VALUES (50, 1, 99, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (51, 2, 99, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (52, 3, 99, '', 'Sol de Domselaar', 1);
INSERT INTO `infolote` VALUES (53, 4, 99, '', 'Sol de Domselaar', 1);
INSERT INTO `infolote` VALUES (54, 5, 99, '', 'Sol de Domselaar', 1);
INSERT INTO `infolote` VALUES (55, 6, 99, '', 'Sol de Domselaar', 1);
INSERT INTO `infolote` VALUES (56, 7, 99, '', 'Sol de Domselaar', 1);
INSERT INTO `infolote` VALUES (57, 8, 99, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (58, 9, 99, '', 'Sol de Domselaar', 1);
INSERT INTO `infolote` VALUES (59, 31, 99, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (60, 32, 99, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (61, 1, 100, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (62, 2, 100, '', 'Sol de Domselaar', 1);
INSERT INTO `infolote` VALUES (63, 3, 100, '', 'Sol de Domselaar', 1);
INSERT INTO `infolote` VALUES (64, 4, 100, '', 'Sol de Domselaar', 1);
INSERT INTO `infolote` VALUES (65, 8, 100, '', 'Sol de Domselaar', 1);
INSERT INTO `infolote` VALUES (66, 9, 100, '', 'Sol de Domselaar', 1);
INSERT INTO `infolote` VALUES (67, 10, 100, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (68, 11, 100, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (69, 12, 100, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (70, 13, 100, '', 'Sol de Domselaar', 1);
INSERT INTO `infolote` VALUES (71, 14, 100, '', 'Sol de Domselaar', 1);
INSERT INTO `infolote` VALUES (72, 26, 100, '', 'Sol de Domselaar', 1);
INSERT INTO `infolote` VALUES (73, 27, 100, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (74, 28, 100, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (75, 29, 100, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (76, 30, 100, '', 'Sol de Domselaar', 1);
INSERT INTO `infolote` VALUES (77, 31, 100, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (78, 32, 100, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (79, 8, 101, '', 'Sol de Domselaar', 1);
INSERT INTO `infolote` VALUES (80, 9, 101, '', 'Sol de Domselaar', 1);
INSERT INTO `infolote` VALUES (81, 10, 101, '', 'Sol de Domselaar', 1);
INSERT INTO `infolote` VALUES (82, 11, 101, '', 'Sol de Domselaar', 1);
INSERT INTO `infolote` VALUES (83, 12, 101, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (84, 13, 101, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (85, 14, 101, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (86, 15, 101, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (87, 17, 101, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (88, 18, 101, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (89, 19, 101, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (90, 20, 101, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (91, 21, 101, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (92, 22, 101, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (93, 23, 101, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (95, 25, 101, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (96, 26, 101, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (97, 27, 101, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (98, 28, 101, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (99, 29, 101, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (100, 30, 101, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (101, 31, 101, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (102, 32, 101, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (103, 8, 102, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (104, 9, 102, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (105, 10, 102, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (106, 11, 102, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (107, 12, 102, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (108, 13, 102, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (109, 14, 102, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (110, 15, 102, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (111, 17, 102, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (112, 18, 102, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (113, 19, 102, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (114, 20, 102, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (115, 21, 102, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (116, 22, 102, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (117, 23, 102, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (118, 25, 102, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (119, 26, 102, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (120, 27, 102, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (121, 28, 102, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (122, 29, 102, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (123, 30, 102, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (124, 31, 102, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (125, 32, 102, '', 'Sol de Domselaar', 0);
INSERT INTO `infolote` VALUES (126, 1, 18, 'f', 'Oasis', 0);
INSERT INTO `infolote` VALUES (127, 12, 18, '', 'Oasis', 0);
INSERT INTO `infolote` VALUES (128, 12, 18, '', 'Oasis', 0);
INSERT INTO `infolote` VALUES (129, 12, 18, '', 'Oasis', 0);
INSERT INTO `infolote` VALUES (130, 12, 18, '', 'Oasis', 0);
INSERT INTO `infolote` VALUES (131, 12, 18, '', 'Oasis', 0);
INSERT INTO `infolote` VALUES (132, 12, 18, '', 'Oasis', 0);
INSERT INTO `infolote` VALUES (134, 453, 12, '', 'Los Tilos', 0);

-- ----------------------------
-- Table structure for notas
-- ----------------------------
DROP TABLE IF EXISTS `notas`;
CREATE TABLE `notas`  (
  `notaid` int(11) NOT NULL AUTO_INCREMENT,
  `nota` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `titulo` varchar(26) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `autor` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `notafecha` date NOT NULL,
  PRIMARY KEY (`notaid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of notas
-- ----------------------------
INSERT INTO `notas` VALUES (1, 'Prueba de notificacion #1', 'List group item heading', 'Jose', '2020-10-28');
INSERT INTO `notas` VALUES (2, 'See and talk to your users and leads immediately by importing your data into the Front Dashboard platform.', 'Import data into Front...', 'Susana', '2020-10-28');

-- ----------------------------
-- Table structure for tipopago
-- ----------------------------
DROP TABLE IF EXISTS `tipopago`;
CREATE TABLE `tipopago`  (
  `tipoid` int(11) NOT NULL AUTO_INCREMENT,
  `pago` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `pinfo` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`tipoid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tipopago
-- ----------------------------
INSERT INTO `tipopago` VALUES (1, 'transferencia', 'CVU<br>\r\n0000000000<br>');
INSERT INTO `tipopago` VALUES (2, 'cheque', 'Cheque de caja');

-- ----------------------------
-- Table structure for usuario
-- ----------------------------
DROP TABLE IF EXISTS `usuario`;
CREATE TABLE `usuario`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `email` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `pass` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `telefono` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `dni` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nivel` int(1) NOT NULL,
  `avatar` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `fecha` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 34 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of usuario
-- ----------------------------
INSERT INTO `usuario` VALUES (1, 'sebastian', 'stn@mail.com', '1234', NULL, NULL, 1, NULL, NULL);
INSERT INTO `usuario` VALUES (19, 'Alexis', 'alexistomaselli@gmail.com', 'alexis', '124578', NULL, 3, 'img/avatar.jpg', '2020-11-25');
INSERT INTO `usuario` VALUES (20, 'Dario', 'alexisstomaselli@gmail.com', 'alexis', '124578', NULL, 3, 'img/avatar.jpg', '2020-11-25');
INSERT INTO `usuario` VALUES (21, 'cliente2', 'alexistomaselliiii@gmail.com', '12345', '12345678', '33333333', 3, 'img/avatar.jpg', '2020-11-27');
INSERT INTO `usuario` VALUES (22, 'gsfgh', 'alexistomaselli@gmail.com', 'alexis', '563543', '5636453', 3, 'img/avatar.jpg', '2020-11-28');
INSERT INTO `usuario` VALUES (23, 'sdfbhs', 'alexistomaselli@gmail.com', 'alexis', '534', '578564', 3, 'img/avatar.jpg', '2020-11-28');
INSERT INTO `usuario` VALUES (24, 'jdgjg', 'alexistomaselli@gmail.com', 'alexis', '456343', '43456346', 3, 'img/avatar.jpg', '2020-11-28');
INSERT INTO `usuario` VALUES (25, 'jdgjg', 'alexistomaselli@gmail.com', 'alexis', '456343', '43456346', 3, 'img/avatar.jpg', '2020-11-28');
INSERT INTO `usuario` VALUES (26, 'gf', 'alexistomaselli@gmail.com', 'alexis', '45648', '24563453', 3, 'img/avatar.jpg', '2020-11-28');
INSERT INTO `usuario` VALUES (27, 'gsdfgh', 'alexistomaselli@gmail.com', 'alexis', '4534', '5436345634', 3, 'img/avatar.jpg', '2020-11-28');
INSERT INTO `usuario` VALUES (28, 'gsdfgh', 'alexistomaselli@gmail.com', 'alexis', '4534', '5436345634', 3, 'img/avatar.jpg', '2020-11-28');
INSERT INTO `usuario` VALUES (29, 'edthdt', 'alexistomaselli@gmail.com', 'alexis', '5463', '3434', 3, 'img/avatar.jpg', '2020-11-28');
INSERT INTO `usuario` VALUES (30, 'tqwet', 'alexistomaselli@gmail.com', 'alexis', '2453', '34343', 3, 'img/avatar.jpg', '2020-11-28');
INSERT INTO `usuario` VALUES (31, 'tqwet', 'alexistomaselli@gmail.com', 'alexis', '2453', '34343', 3, 'img/avatar.jpg', '2020-11-28');
INSERT INTO `usuario` VALUES (32, 'gsgh', 'alexistomaselli@gmail.com', 'alexis', '486486', '4634634', 3, 'img/avatar.jpg', '2020-11-28');
INSERT INTO `usuario` VALUES (33, 'grhyrhr', 'alexistomaselli@gmail.com', 'alexis', '2532453', '4524534', 3, 'img/avatar.jpg', '2020-11-28');

-- ----------------------------
-- Table structure for usuario_copy1
-- ----------------------------
DROP TABLE IF EXISTS `usuario_copy1`;
CREATE TABLE `usuario_copy1`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `email` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `pass` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `nivel` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of usuario_copy1
-- ----------------------------
INSERT INTO `usuario_copy1` VALUES (1, 'sebastian', 'stn@mail.com', '1234', '1');

-- ----------------------------
-- Table structure for vendedor
-- ----------------------------
DROP TABLE IF EXISTS `vendedor`;
CREATE TABLE `vendedor`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `dni` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `email` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `telefono` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `avatar` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 39 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of vendedor
-- ----------------------------
INSERT INTO `vendedor` VALUES (34, 'aaaaa', 'a@a.com', 'aaaaaa', '', 'img/avatar.jpg');
INSERT INTO `vendedor` VALUES (35, 'vendedor 2', 'a@a.com', '30256458', '', 'img/avatar.jpg');
INSERT INTO `vendedor` VALUES (36, 'vendedor 2', 'a@a.com', '33554488', '464546', 'img/avatar.jpg');
INSERT INTO `vendedor` VALUES (37, 'fasga', 'a@a.com', '654456', '', 'img/avatar.jpg');
INSERT INTO `vendedor` VALUES (38, 'fasga', 'a@a.com', '654456', '', 'img/avatar.jpg');

-- ----------------------------
-- Table structure for venta
-- ----------------------------
DROP TABLE IF EXISTS `venta`;
CREATE TABLE `venta`  (
  `id` int(11) NOT NULL,
  `id_cliente` int(11) NULL DEFAULT NULL,
  `id_lote` int(11) NULL DEFAULT NULL,
  `id_vendedor` int(11) NULL DEFAULT NULL,
  `moneda` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `fecha` date NULL DEFAULT NULL,
  `monto_venta` float NULL DEFAULT NULL,
  `cantidad_cuotas` int(3) NULL DEFAULT NULL,
  `alicuota` int(1) NULL DEFAULT NULL,
  `cuota_refuerzo` int(1) NULL DEFAULT NULL,
  `concepto` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
