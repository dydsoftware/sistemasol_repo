<?php
session_start();
if($_SESSION['logueado'] != "SI"){
header('location: login');
exit();
}

include_once('../mode/block.class.admin.php');

$blok = new Admin();
$id = $_SESSION['usuario'];
$mUser = $blok->getUsuario($id);


?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>San Jorge Construcciones</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha2/css/bootstrap.min.css" integrity="sha384-DhY6onE6f3zzKbjUPRc2hOzGAdEf4/Dz+WJwBvEYL/lkkIsI3ihufq9hk9K4lVoK" crossorigin="anonymous">
<link rel="stylesheet" href="../asset/css/style.css">

<!-- jQuery -->
<!-- <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script> -->
<script src="../asset/js/jquery-1.11.2.min.js"></script>

<!-- DataTables -->
<!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css" class="stylesheet"> -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.2/css/uikit.min.css" class="stylesheet">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.uikit.min.css" class="stylesheet">


<!-- Select2 -->


</head>
<body>

<nav class="navbar navbar-expand-lg navbar-dark bg-darkblue">
<div class="container">
<a class="navbar-brand" href="./">
<img src="../asset/img/logoUno.png" alt="San Jorge" width="30" class="d-inline-block align-top">
San Jorge
</a>
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
<span class="navbar-toggler-icon"></span>
</button>

<div class="collapse navbar-collapse" id="navbarSupportedContent">
<ul class="navbar-nav mr-auto mb-2 mb-lg-0">
<li class="nav-item"><a class="nav-link" aria-current="page" href="./">Inicio</a></li>
<li class="nav-item dropdown">
  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-expanded="false">
  Clientes
  </a>
  <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
  <li><a class="dropdown-item" href="./?page=nuevo.cliente">Nuevo Cliente</a></li>
  <li><a class="dropdown-item" href="./?page=listado.clientes">Listado de Clientes</a></li>
  </ul>
</li>
<li class="nav-item dropdown">
<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-expanded="false">
Expensas
</a>
<ul class="dropdown-menu" aria-labelledby="navbarDropdown">
<li><a class="dropdown-item" href="./?page=nuevo.factura">Crear Factura</a></li>
</ul>
</li>
<li class="nav-item dropdown">
<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-expanded="false">
Lote
</a>
<ul class="dropdown-menu" aria-labelledby="navbarDropdown">
<li><a class="dropdown-item" href="./?page=nuevo.venta">Nueva Venta</a></li>
<li><a class="dropdown-item" href="./?page=nuevo.factura">Crear Factura</a></li>
<li><a class="dropdown-item" href="./?page=nuevo.lote">Nuevo Lote</a></li>
<hr>
<li><a class="dropdown-item" href="./?page=soldedomselaar">Sol de Domselaar</a></li>
<li><a class="dropdown-item" href="./?page=oasis">Oasis</a></li>
<li><a class="dropdown-item" href="./?page=lostilos">Los Tilos</a></li>
</ul>
</li>
<li class="nav-item"><a class="nav-link" href="./?page=salir">Salir</a></li>

</ul>
</div>
</div>
</nav>

<div class="container">

<!--div class="mt-4">
<?php
// echo 'Hola '.$mUser['nombre'];
?>
</div-->

<div class=py-4>
<?php
if(isset($_GET['page']) && $_GET['page']){
  include 'phpfiles/'.$_GET['page'].'.php';
} else {
  include 'phpfiles/home.php';
}
?>
</div>
</div>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha2/js/bootstrap.bundle.min.js" integrity="sha384-BOsAfwzjNJHrJ8cZidOg56tcQWfp6y72vEJ8xQ9w6Quywb24iOsW913URv1IS4GD" crossorigin="anonymous"></script>
<script src="../asset/js/main.js"></script>
<script src="../asset/js/jquery-1.11.2.min.js"></script>
<!-- <script src="../asset/js/jquery.easing.min.js" type="text/javascript"></script>  -->
<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script> -->

<!-- SELECT2 -->
<!-- <link href="../asset/plugins/select2/select2.css" rel="stylesheet" type="text/css" media="screen"/>
<script src="../asset/plugins/select2/select2.min.js" type="text/javascript"></script>
<script src="../asset/plugins/select2/select2_locale_es.js" type="text/javascript"></script> -->
    <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script> -->
    <!-- <script src="../asset/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script>  -->
    <!-- <link href="../asset/plugins/datatables/css/jquery.dataTables.css" rel="stylesheet" type="text/css" media="screen"/>
    <link href="../asset/plugins/datatables/extensions/TableTools/css/dataTables.tableTools.min.css" rel="stylesheet" type="text/css" media="screen"/>
    <link href="../asset/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css" rel="stylesheet" type="text/css" media="screen"/>
    <link href="../asset/plugins/datatables/extensions/Responsive/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet" type="text/css" media="screen"/>  
        
    -->
    <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
   
		
		
		
    <style>
    th.sorting_asc::after{
      display: none !important;
    }
    </style>
</body>
</html>