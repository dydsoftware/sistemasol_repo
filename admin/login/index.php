<?php
session_start();

include_once('../../mode/block.class.admin.php');
$con = new Admin();
$login = $con->login();

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title>San Jorge</title>
        <meta content="Admin Dashboard" name="description" />
        <meta content="Themesbrand" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App Icons -->
        <link rel="shortcut icon" href="../../assets/images/favicon.ico">

        <!--Morris Chart CSS -->
        <link rel="stylesheet" href="../../assets/plugins/morris/morris.css">

        <!-- App css -->
        <link href="../../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../../assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="../../assets/css/style.css" rel="stylesheet" type="text/css" />

    </head>


    <body>

        <!-- Loader -->
        <div id="preloader"><div id="status"><div class="spinner"></div></div></div>

        <div class="accountbg"></div>
        <div class="wrapper-page">

            <div class="card">
                <div class="card-body">

                    <h3 class="text-center m-0">
                        <a href="index.html" class="logo logo-admin"><img src="../../asset/img/logoUno.png" height="50" alt="logo"></a>
                    </h3>

                    <div class="p-3">
                        <h4 class="text-muted font-18 m-b-5 text-center">Bienvenido</h4>
                        

                    <form class="form-horizontal m-t-30" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
                            <div class="form-group">
                                <label for="email">Email</label>
                            <input type="text" class="form-control" id="inputEmail" name="email" placeholder="Ingrese su email">
                            </div>

                            <div class="form-group">
                                <label for="password">Contraseña</label>
                                <input type="password" class="form-control" id="inputPassword" name="pass" placeholder="Ingrese la contraseña">
                            </div>

                            <div class="form-group row m-t-20">
                                <div class="col-sm-12 text-center">
                                    <button class="btn btn-primary w-md waves-effect waves-light" type="submit" name="add">Iniciar sesión</button>
                                </div>
                            </div>

                        </form>
                    </div>

                </div>
            </div>

            <div class="m-t-40 text-center text-light">
                <!-- <p>¿No tenes una cuenta? <a href="mailto:contacto@ruralsalliquelo.com.ar" class="font-500 font-14 text-info font-secondary"> Contactar </a> </p> -->
                <p>©  <?php echo date('Y');?> San Jorge</p>
            </div>

        </div>




        <!-- jQuery  -->
        <script src="../../assets/js/jquery.min.js"></script>
        <script src="../../assets/js/bootstrap.bundle.min.js"></script>
        <script src="../../assets/js/modernizr.min.js"></script>
        <script src="../../assets/js/waves.js"></script>
        <script src="../../assets/js/jquery.slimscroll.js"></script>
        <script src="../../assets/js/jquery.nicescroll.js"></script>
        <script src="../../assets/js/jquery.scrollTo.min.js"></script>

        <!--Morris Chart-->


    

        <!-- App js -->
        <script src="../../assets/js/app.js"></script>

    </body>
</html>