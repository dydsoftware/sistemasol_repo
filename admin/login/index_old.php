<?php
session_start();

include_once('../../mode/block.class.admin.php');
$con = new Admin();
$login = $con->login();

?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>San Jorge Construcciones</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha2/css/bootstrap.min.css" integrity="sha384-DhY6onE6f3zzKbjUPRc2hOzGAdEf4/Dz+WJwBvEYL/lkkIsI3ihufq9hk9K4lVoK" crossorigin="anonymous">
<link rel="stylesheet" href="../../asset/css/style.css">
</head>
<body class="body-login text-center">

<main class="form-signin">
<img class="mb-4" src="../../asset/img/logoUno.png" alt="San Jorge Construcciones" width="72">
<h3 class="mb-4">Login</h3>

<form action="" method="post">
<label for="inputEmail" class="visually-hidden">Email</label>
<input type="email" id="inputEmail" class="form-control" name="email" placeholder="Email" required autofocus>
<label for="inputPassword" class="visually-hidden">Contrase&ntilde;a</label>
<input type="password" id="inputPassword" class="form-control" name="pass" placeholder="Contrase&ntilde;a" required>

<button type="submit" class="btn btn-primary-sj mt-4" name="add">Ingresar</button>

<p class="mt-5 mb-3 text-muted small">San Jorge Construcciones - <?=date('Y');?></p>
</form>

</main>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha2/js/bootstrap.bundle.min.js" integrity="sha384-BOsAfwzjNJHrJ8cZidOg56tcQWfp6y72vEJ8xQ9w6Quywb24iOsW913URv1IS4GD" crossorigin="anonymous"></script>
</body>
</html>