<?php
session_start();
if($_SESSION['logueado'] != "SI"){
header('location: login');
exit();
}

include_once('../mode/block.class.admin.php');

$blok = new Admin();
$id = $_SESSION['usuario'];
$mUser = $blok->getUsuario($id);
//print_r($mUser);

$titulo_array = array(
    '<i class="dripicons-user-id"></i> Nuevo Cliente' => 'nuevo.cliente',
    '<i class="dripicons-user-id"></i> Listado de Clientes' => 'listado.clientes',
    '<i class="dripicons-user-id"></i> Nuevo Lote' => 'nuevo.lote',
    '<i class="dripicons-user-id"></i> Listado de Lotes' => 'listado.lotes',
    '<i class="ti-bar-chart"></i> Estadísticas del Barrio' => 'info.barrio',
    '<i class="ti-bar-chart"></i> Ventas' => 'nuevo.venta',
    '<i class="fa fa-address-book"></i> Nuevo Vendedor' => 'nuevo.vendedor',
    '<i class="fa fa-address-book"></i> Listado de Vendedores' => 'listado.vendedores',
    'Inicio' => ''
);

/*Armo el título de la página*/
if(isset($_GET['page']) && $_GET['page']){
    foreach($titulo_array as $t){
        
        $titulo = array_search($t,$titulo_array);

        if($t == $_GET['page']){
        break;
        }

    }
  } else {
   $titulo = 'Inicio';
  }


?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title>San Jorge</title>
    <meta content="Sistema San Jorge" name="description" />
    <meta content="Themesbrand" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!-- App Icons -->
    <link rel="shortcut icon" type="image/png" href="../asset/img/logoUno.png" sizes="64x64">

    <!-- SweetAlert -->
    <link href="../assets/plugins/sweet-alert2/sweetalert2.min.css" rel="stylesheet" type="text/css">
    <!--Morris Chart CSS -->
    <link rel="stylesheet" href="../assets/plugins/morris/morris.css">

    <!-- jvectormap -->
    <link href="../assets/plugins/jvectormap/jquery-jvectormap-2.0.2.css" rel="stylesheet">

    <!-- App css -->
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />

    <link href="../assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
    <script src="../assets/js/jquery.min.js"></script>
    <!-- <script src="../asset/js/jquery-1.11.2.min.js"></script> -->
    <script src="../asset/js/main.js"></script>

</head>


<body>

    <!-- Loader -->
    <div id="preloader">
        <div id="status">
            <div class="spinner"></div>
        </div>
    </div>

    <div class="header-bg">
        <!-- Navigation Bar-->
        <header id="topnav">
            <div class="topbar-main">
                <div class="container-fluid">

                    <!-- Logo container-->
                    <div class="logo">
                        <!-- Image Logo -->
                        <a href="index.html" class="logo">
                            <img src="../asset/img/logoUno.png" alt="" height="40" class="logo-small">
                            <img src="../asset/img/logoUno.png" alt="" height="50" class="logo-large">
                       
                                San Jorge
                        </a>

                    </div>
                    <!-- End Logo container-->


                    <div class="menu-extras topbar-custom">

                        <ul class="list-inline float-right mb-0">

                            <!-- notification-->
                            <li class="list-inline-item dropdown notification-list">
                                <a class="nav-link dropdown-toggle arrow-none waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                    <i class="ti-bell noti-icon"></i>
                                    <span class="badge badge-info badge-pill noti-icon-badge">3</span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-menu-lg">
                                    <!-- item-->
                                    <div class="dropdown-item noti-title">
                                        <h5>Notification (3)</h5>
                                    </div>

                                    <!-- item-->
                                    <!-- <a href="javascript:void(0);" class="dropdown-item notify-item active">
                                        <div class="notify-icon bg-success"><i class="mdi mdi-cart-outline"></i></div>
                                        <p class="notify-details"><b>Your order is placed</b><small class="text-muted">Dummy text of the printing and typesetting industry.</small></p>
                                    </a> -->

                                    <!-- item-->
                                    <!-- <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <div class="notify-icon bg-warning"><i class="mdi mdi-message"></i></div>
                                        <p class="notify-details"><b>New Message received</b><small class="text-muted">You have 87 unread messages</small></p>
                                    </a> -->

                                    <!-- item-->
                                    <!-- <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <div class="notify-icon bg-info"><i class="mdi mdi-martini"></i></div>
                                        <p class="notify-details"><b>Your item is shipped</b><small class="text-muted">It is a long established fact that a reader will</small></p>
                                    </a> -->

                                    <!-- All-->
                                    <!-- <a href="javascript:void(0);" class="dropdown-item notify-item">
                                            View All
                                        </a> -->

                                </div>
                            </li>
                            <!-- User-->
                            <li class="list-inline-item dropdown notification-list">
                                <a class="nav-link dropdown-toggle arrow-none waves-effect nav-user" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                    <img src="../asset/img/avatar.jpg" alt="user" class="rounded-circle">
                                    <span class="ml-1"><?php echo $mUser['nombre'];?><i class="mdi mdi-chevron-down"></i> </span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                                    <!-- <a class="dropdown-item" href="#"><i class="dripicons-user text-muted"></i> Profile</a>
                                    <a class="dropdown-item" href="#"><i class="dripicons-wallet text-muted"></i> My Wallet</a>
                                    <a class="dropdown-item" href="#"><span class="badge badge-success pull-right m-t-5">5</span><i class="dripicons-gear text-muted"></i> Settings</a>
                                    <a class="dropdown-item" href="#"><i class="dripicons-lock text-muted"></i> Lock screen</a>
                                    <div class="dropdown-divider"></div> -->
                                    <a class="dropdown-item" href="./?page=salir"><i class="dripicons-exit text-muted"></i> Salir</a>
                                </div>
                            </li>
                            <li class="menu-item list-inline-item">
                                <!-- Mobile menu toggle-->
                                <a class="navbar-toggle nav-link">
                                    <div class="lines">
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                    </div>
                                </a>
                                <!-- End mobile menu toggle-->
                            </li>

                        </ul>
                    </div>
                    <!-- end menu-extras -->

                    <div class="clearfix"></div>

                </div>
                <!-- end container -->
            </div>
            <!-- end topbar-main -->

            <!-- MENU Start -->
            <div class="navbar-custom">
                <div class="container-fluid">
                    <div id="navigation">
                        <!-- Navigation Menu-->
                        <ul class="navigation-menu">
                        <?php if($_SESSION['nivel'] == 1) { ?>

                            <li class="has-submenu">
                                <a href="index.php"><i class="dripicons-device-desktop"></i>Inicio</a>
                            </li>

                            <li class="has-submenu">
                                <a href="#"><i class="dripicons-user-id"></i>Clientes <i class="mdi mdi-chevron-down mdi-drop"></i></a>
                                <ul class="submenu megamenu">
                                    <li>
                                        <ul>
                                            <li><a href="./?page=nuevo.cliente">Nuevo Cliente</a></li>
                                            <li><a href="./?page=listado.clientes">Listar Clientes</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>

                            <li class="has-submenu">
                                <a href="#"><i class="dripicons-user-id"></i>Vendedores <i class="mdi mdi-chevron-down mdi-drop"></i></a>
                                <ul class="submenu megamenu">
                                    <li>
                                        <ul>
                                            <li><a href="./?page=nuevo.vendedor">Nuevo Vendedor</a></li>
                                            <li><a href="./?page=listado.vendedores">Listar Vendedores</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>

                            <li class="has-submenu">
                                <a href="#"><i class="dripicons-to-do"></i>Expensas <i class="mdi mdi-chevron-down mdi-drop"></i></a>
                                <ul class="submenu">
                                    <li>
                                        <a href="#">Crear Factura</a>
                                    </li>
                                </ul>
                            </li>

                            <li class="has-submenu">
                                <a href="#"><i class="ti-map"></i>Lotes <i class="mdi mdi-chevron-down mdi-drop"></i></a>
                                <ul class="submenu">
                                    <li><a href="./?page=nuevo.lote">Crear lote</a></li>
                                    <li><a href="./?page=listado.lotes">Listar lotes</a></li>
                                    <hr>
                                    <li><span class="text-muted">Barrios</span></li>
                                    <li><a href="./?page=info.barrio&barrio=Sol%20de%20Domselaar">Sol de Domselaar</a></li>
                                    <li><a href="./?page=info.barrio&barrio=Los%20Tilos">Los Tilos</a></li>
                                    <li><a href="./?page=info.barrio&barrio=Oasis">Oasis</a></li>
                                </ul>
                            </li>

                            <li class="has-submenu">
                                <a href="#"><i class="dripicons-copy"></i>Ventas <i class="mdi mdi-chevron-down mdi-drop"></i></a>
                                <ul class="submenu">
                                    <li>
                                        <ul>
                                            <li><a href="./?page=nuevo.venta">Nueva Venta</a></li>
                                            <!-- <li><a href="pages-invoice.html">Invoice</a></li>
                                            <li><a href="pages-directory.html">Directory</a></li>
                                            <li><a href="pages-login.html">Login</a></li>
                                            <li><a href="pages-register.html">Register</a></li> -->
                                        </ul>
                                    </li>
                                    <li>
                                        <ul>
                                            <!-- <li><a href="pages-recoverpw.html">Recover Password</a></li>
                                            <li><a href="pages-lock-screen.html">Lock Screen</a></li>
                                            <li><a href="pages-blank.html">Blank Page</a></li>
                                            <li><a href="pages-404.html">Error 404</a></li>
                                            <li><a href="pages-500.html">Error 500</a></li> -->
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                           <?php } ?>

                        </ul>
                        <!-- End navigation menu -->
                    </div>
                    <!-- end #navigation -->
                </div>
                <!-- end container -->
            </div>
            <!-- end navbar-custom -->
        </header>
        <!-- End Navigation Bar-->

        <div class="container-fluid">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <!-- <form class="float-right app-search">
                            <input type="text" placeholder="Search..." class="form-control">
                            <button type="submit"><i class="fa fa-search"></i></button>
                        </form> -->
                        <h4 id="titulo-pagina" class="page-title"> <?php echo $titulo;?></h4>
                    </div>
                </div>
            </div>
            <!-- end page title end breadcrumb -->

            <!-- <div class="row">
                <div class="col-12 mb-4">
                    <div id="morris-bar-example" class="dash-chart"></div>

                    <div class="mt-4 text-center">
                        <button type="button" class="btn btn-outline-light ml-1 waves-effect waves-light">Year 2017</button>
                        <button type="button" class="btn btn-outline-info ml-1 waves-effect waves-light">Year 2018</button>
                        <button type="button" class="btn btn-outline-light ml-1 waves-effect waves-light">Year 2019</button>
                    </div>
                </div>
            </div> -->
        </div>
    </div>


    <div class="wrapper">
    <?php
    if($_SESSION['nivel'] == 1) {
        if(isset($_GET['page']) && $_GET['page']){
        include 'phpfiles/'.$_GET['page'].'.php';
        } else {
        include 'phpfiles/home.php';
        }
    }else {
        // echo '<script>
        //         window.location.href = "../template_admin/pages-lock-screen.html";
        //     </script>' ;
        echo '<script>
                window.location.href = "login";
            </script>' ;
        // header('location: ../template_admin/pages-lock-screen.html');
    }
?>
        
        <!-- end container -->
    </div>
    <!-- end wrapper -->


    <!-- Footer -->
    <footer class="footer text-white header-bg">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    © <?php echo date('Y')?> SAN JORGE
                </div>
            </div>
        </div>
    </footer>
    <!-- End Footer -->


    <!-- jQuery  -->
    
    <script src="../assets/js/bootstrap.bundle.min.js"></script>
    <script src="../assets/js/modernizr.min.js"></script>
    <script src="../assets/js/waves.js"></script>
    <script src="../assets/js/jquery.slimscroll.js"></script>
    <script src="../assets/js/jquery.nicescroll.js"></script>
    <script src="../assets/js/jquery.scrollTo.min.js"></script>
    <!-- Datatable -->
    <script src="../assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="../assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>

    <!-- SweetAlert -->
    <script src="../assets/plugins/sweet-alert2/sweetalert2.min.js"></script>
    <script src="../assets/pages/sweet-alert.init.js"></script>
    <!--Morris Chart-->
    <script src="../assets/plugins/morris/morris.min.js"></script>
    <script src="../assets/plugins/raphael/raphael-min.js"></script>

    <script src="../assets/pages/dashboard.js"></script>


    <!-- App js -->
    <script src="../assets/js/app.js"></script>

    

    <!-- google maps api -->
    <script src="http://maps.google.com/maps/api/js?key=AIzaSyCtSAR45TFgZjOs4nBFFZnII-6mMHLfSYI"></script>

    <!-- Gmaps file -->
    <script src="../assets/plugins/gmaps/gmaps.min.js"></script>
    <!-- demo codes -->
    <script src="../assets/pages/gmaps.js"></script>

    <script src="../assets/plugins/select2/js/select2.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/select2/js/i18n/es.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<!-- Libreria español -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/i18n/es.js"></script>

    <!-- <script src="../asset/plugins/select2/select2.min.js" type="text/javascript"></script> -->

</body>
</html>