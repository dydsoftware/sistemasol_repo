<?php
include_once('../mode/block.class.admin.php');
$blok = new Admin();

$lista = $blok->getListaBarrios();
?>

<h5>Sol de Domselaar</h5>

<table class="table">
<thead>
<tr>
<th>id</th>
<th>Nombre y Apqllido</th>
<th>Lote</th>
<th>Manzana</th>
<th>Utilidades</th>
</tr>
</thead>
<tbody>
<?php
foreach($lista as $c){
if($c['barrio'] == 'Sol de Domselaar'){
?>
<tr>
<td><?=$c['idb'];?></td>
<td><?=$c['nya'];?></td>
<td><?=$c['lotenumero'];?></td>
<td><?=$c['manzana'];?></td>
<td>
<a href="./?page=detalle&id=<?=$c['idb'];?>" class="btn btn-outline-primary btn-sm">Detalles</a>
</td>
</tr>
<?php
}
}
?>
</tbody>
</table>