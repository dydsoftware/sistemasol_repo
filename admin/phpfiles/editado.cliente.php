<?php

include_once('../mode/block.class.admin.php');

$cliente = new Admin();

$datos = $cliente->getCliente($id);
print_r($datos);

if(isset($_POST['submit'])){

    $data = array(
            $_POST['nya'],
            $_POST['email'],
            $_POST['pass'],
            'img/avatar.jpg',
            $_POST['telefono'],
            3,
            date('Y-m-d'),
            $_POST['dni']
            );

    $msg = $cliente->setNuevoCliente($data);
 
}

?>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card m-b-20">
                <div class="card-body">
                    <h4 class="mt-0 header-title">Editar cliente</h4>
                    <hr>
                    <form action="" method="POST">
                        <div class="form-group row">
                            <label for="nya" class="col-sm-2 col-form-label">Nombre y apellido</label>
                            <div class="col-sm-10">
                                <input class="form-control" type="text" name="nya" value="<?php echo $datos['nya'] ?>" id="nya" placeholder="Nombre y apellido" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="dni" class="col-sm-2 col-form-label">Documento</label>
                            <div class="col-sm-10">
                                <input class="form-control" type="text" name="dni" value="<?php echo $datos['dni'] ?>" id="dni" placeholder="Documento de identidad" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="email" class="col-sm-2 col-form-label">Email</label>
                            <div class="col-sm-10">
                                <input class="form-control" value="<?php echo $datos['email'] ?>" type="email" name="email" id="email" placeholder="Email" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="pass" class="col-sm-2 col-form-label">Contraseña</label>
                            <div class="col-sm-10">
                                <input class="form-control" value="<?php echo $datos['pass'] ?>" type="password" name="pass" placeholder="Contrase&ntilde;a" id="pass">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="telefono" class="col-sm-2 col-form-label">Teléfono (opcional)</label>
                            <div class="col-sm-10">
                                <input class="form-control" type="number" name="telefono" value="<?php echo $datos['telefono'] ?>" id="telefono" placeholder="Teléfono">
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-md-10"></div>
                            <div class="float-right col-md-2">
                              <button type="submit" name="submit" style="text-align:right !important;" class="btn btn-primary float-right">Agregar</button>
                            </div>
                        </div>
                        
                    </form>
                </div>
            </div>
        </div> <!-- end col -->
    </div>

</div>

<script src="../assets/plugins/sweet-alert2/sweetalert2.min.js"></script>
<script src="../assets/pages/sweet-alert.init.js"></script>
<script>
    $( document ).ready(function() {

        <?php if(isset($_POST['submit'])){ ?>
            fireSuccess('<?php echo$msg?>')
        <?php }?>

});
</script>