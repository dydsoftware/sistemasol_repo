<?php
include_once('../mode/block.class.admin.php');
$blok = new Admin();

$lista = $blok->getListaLotesFiltro(array());
//print_r($lista);


?>
<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="card m-b-30">
                <div class="card-body">
                    <h4 class="mt-0 header-title">Listado de Lotes</h4>
                    <table class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;" id="datatable">
                        <thead>
                            <tr>
                                <th>Numero Lote</th>
                                <th>Manzana</th>
                                <th>Barrio</th>
                                <th>Estado</th>
                                <th>Eliminar</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
    <script src="../assets/plugins/sweet-alert2/sweetalert2.min.js"></script>
    <script src="../assets/pages/sweet-alert.init.js"></script>
<script>
    $( document ).ready(function() {

        listar_lotes();

    });
</script>