<div class="container-fluid">

                <div class="row">
                <div class="col-lg-6">
                        <div class="card m-b-20">
                            <div class="card-body">

                                <h4 class="mt-0 header-title">MAPA</h4>
                                <p class="text-muted m-b-30 font-14">Lotes en el mapa</p>

                                <div id="gmaps-markers" class="gmaps"></div>
                            </div>
                        </div>
                    </div> <!-- end col -->
                    <div class="col-lg-6">
                        <div class="card m-b-30">
                            <div class="card-body">
                                <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                    <label class="btn btn-success active">
                                        <input type="radio" name="options" id="option1" autocomplete="off" checked=""> Nueva Venta
                                    </label>
                                    <label class="btn btn-secondary">
                                        <input type="radio" name="options" id="option3" autocomplete="off"> Cargar Factura
                                    </label>
                                </div>
                                <hr>
                                <h4 class="mt-0 header-title">Estados de lotes</h4>
                                <!-- <p class="text-muted m-b-30 font-14">Example of vector map.</p> -->
                                <div class="table-responsive">
                                    <table class="table m-t-20 mb-0 table-vertical">

                                        <tbody>
                                            <tr>
                                                <td><i class="mdi mdi-checkbox-blank-circle text-primary"></i>  Disponible</td>
                                                <td>
                                                    -
                                                    <p class="m-0 text-muted font-14">Cliente</p>
                                                </td>
                                                <td>
                                                    -
                                                    <p class="m-0 text-muted font-14">Fecha</p>
                                                </td>
                                            </tr>

                                            <tr>
                                            <td><i class="mdi mdi-checkbox-blank-circle text-primary"></i>  Disponible</td>
                                                <td>
                                                    -
                                                    <p class="m-0 text-muted font-14">Cliente</p>
                                                </td>
                                                <td>
                                                    -
                                                    <p class="m-0 text-muted font-14">Fecha</p>
                                                </td>
                                            </tr>

                                            <tr>
                                            <td><i class="mdi mdi-checkbox-blank-circle text-danger"></i> No Disponible </td>
                                                <td>
                                                Santiago Donnet
                                                    <p class="m-0 text-muted font-14">Cliente</p>
                                                </td>
                                                <td>
                                                    10/10/2020
                                                    <p class="m-0 text-muted font-14">Fecha</p>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td><i class="mdi mdi-checkbox-blank-circle text-danger"></i>  No Disponible</td>
                                                <td>
                                                    Alberto Sánchez
                                                    <p class="m-0 text-muted font-14">Cliente</p>
                                                </td>
                                                <td>
                                                    7/09/2016
                                                    <p class="m-0 text-muted font-14">Fecha</p>
                                                </td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div> <!-- end row -->


            </div> <!-- end container -->

            <!-- agrego el barrio al título de la página del index -->
            <script>
                $('#titulo-pagina').html('<?php echo $titulo.' - '.$_GET['barrio']?>')
            </script>