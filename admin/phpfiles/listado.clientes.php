<?php
include_once('../mode/block.class.admin.php');
$blok = new Admin();

$lista = $blok->getListaClientesFiltro(array());

if(isset($_POST['submit'])){
    $data = array(
            $_POST['nya'],
            $_POST['email'],
            $_POST['telefono'],
            $_POST['dni']
            );
    $id = $_POST['hidden'];
    $blok->updateCliente($data,$id);
}

?>
<div class="container">
            <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title mt-0">Detalle del cliente</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <!-- <div class="col-6">
                                    <p>Hola</p>
                                </div> -->
                                <div class="col-12">
                                <div class="card-body">
                            <div class="col-md-8">
                            <h4 class="mt-0 header-title">Plan en cuotas - <span class="text-muted">Progreso por lote</span></h4>
                            </div>

                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs nav-tabs-custom nav-justified" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active show" data-toggle="tab" href="#home2" role="tab" aria-selected="false">Lote 1 - Sol de Domselaar</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#profile2" role="tab" aria-selected="false">Lote 2 - Oasis</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#messages2" role="tab" aria-selected="true">Lote 3 - Los Tilos</a>
                                </li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane p-3 active show" id="home2" role="tabpanel">
                                <div class="card m-b-30">
                                      

                                            <div class="table-responsive">
                                                <table class="table m-t-20 mb-0 table-vertical">

                                                    <tbody>
                                                        <tr>
                                                            <td><i class="mdi mdi-checkbox-blank-circle text-primary"></i> Pago Confirmado</td>
                                                            <td>
                                                                $14,584
                                                                <p class="m-0 text-muted font-14">Monto</p>
                                                            </td>
                                                            <td>
                                                                5/12/2020
                                                                <p class="m-0 text-muted font-14">Fecha</p>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                        <td><i class="mdi mdi-checkbox-blank-circle text-primary"></i> Pago Confirmado</td>
                                                            <td>
                                                                $8,541
                                                                <p class="m-0 text-muted font-14">Monto</p>
                                                            </td>
                                                            <td>
                                                                10/11/2020
                                                                <p class="m-0 text-muted font-14">Fecha</p>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                        <td><i class="mdi mdi-checkbox-blank-circle text-warning"></i> Esperando Pago</td>
                                                            <td>
                                                                $954
                                                                <p class="m-0 text-muted font-14">Monto</p>
                                                            </td>
                                                            <td>
                                                                10/10/2020
                                                                <p class="m-0 text-muted font-14">Fecha</p>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td><i class="mdi mdi-checkbox-blank-circle text-danger"></i> Pago Atrasado</td>
                                                            <td>
                                                                $44,584
                                                                <p class="m-0 text-muted font-14">Monto</p>
                                                            </td>
                                                            <td>
                                                                7/09/2016
                                                                <p class="m-0 text-muted font-14">Fecha</p>
                                                            </td>
                                                        </tr>

                                                    </tbody>
                                                </table>
                                            </div>
                                            <hr>
                                            <h4 class="mt-0 header-title">PROGRESO</h4>
                                            <div class="container">
                                                <div class="progress m-b-30">
                                                <div class="progress-bar" role="progressbar" style="width: 40%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">40%</div>
                                                </div>
                                            </div>
                                       
                                    </div>
                                </div>
                                <div class="tab-pane p-3" id="profile2" role="tabpanel">
                                    <div class="card m-b-30">
                                       

                                            <div class="table-responsive">
                                                <table class="table m-t-20 mb-0 table-vertical">

                                                    <tbody>
                                                        <tr>
                                                            <td><i class="mdi mdi-checkbox-blank-circle text-primary"></i> Pago Confirmado</td>
                                                            <td>
                                                                $14,584
                                                                <p class="m-0 text-muted font-14">Monto</p>
                                                            </td>
                                                            <td>
                                                                5/12/2020
                                                                <p class="m-0 text-muted font-14">Fecha</p>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                        <td><i class="mdi mdi-checkbox-blank-circle text-primary"></i> Pago Confirmado</td>
                                                            <td>
                                                                $8,541
                                                                <p class="m-0 text-muted font-14">Monto</p>
                                                            </td>
                                                            <td>
                                                                10/11/2020
                                                                <p class="m-0 text-muted font-14">Fecha</p>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                        <td><i class="mdi mdi-checkbox-blank-circle text-warning"></i> Esperando Pago</td>
                                                            <td>
                                                                $954
                                                                <p class="m-0 text-muted font-14">Monto</p>
                                                            </td>
                                                            <td>
                                                                8/11/2020
                                                                <p class="m-0 text-muted font-14">Fecha</p>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td><i class="mdi mdi-checkbox-blank-circle text-danger"></i> Pago Atrasado</td>
                                                            <td>
                                                                $44,584
                                                                <p class="m-0 text-muted font-14">Monto</p>
                                                            </td>
                                                            <td>
                                                                7/11/2016
                                                                <p class="m-0 text-muted font-14">Fecha</p>
                                                            </td>
                                                        </tr>

                                                    </tbody>
                                                </table>
                                            </div>
                                            <hr>
                                            <h4 class="mt-0 header-title">PROGRESO</h4>
                                            <div class="container">
                                                <div class="progress m-b-30">
                                                <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25%</div>
                                                </div>
                                            </div>
                                    
                                    </div>
                                </div>
                                <div class="tab-pane p-3" id="messages2" role="tabpanel">
                                <div class="card m-b-30">
                                  
                                            <div class="table-responsive">
                                                <table class="table m-t-20 mb-0 table-vertical">

                                                    <tbody>
                                                        <tr>
                                                            <td><i class="mdi mdi-checkbox-blank-circle text-primary"></i> Pago Confirmado</td>
                                                            <td>
                                                                $14,584
                                                                <p class="m-0 text-muted font-14">Monto</p>
                                                            </td>
                                                            <td>
                                                                5/12/2020
                                                                <p class="m-0 text-muted font-14">Fecha</p>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                        <td><i class="mdi mdi-checkbox-blank-circle text-primary"></i> Pago Confirmado</td>
                                                            <td>
                                                                $8,541
                                                                <p class="m-0 text-muted font-14">Monto</p>
                                                            </td>
                                                            <td>
                                                                10/11/2020
                                                                <p class="m-0 text-muted font-14">Fecha</p>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                        <td><i class="mdi mdi-checkbox-blank-circle text-warning"></i> Esperando Pago</td>
                                                            <td>
                                                                $954
                                                                <p class="m-0 text-muted font-14">Monto</p>
                                                            </td>
                                                            <td>
                                                                8/11/2020
                                                                <p class="m-0 text-muted font-14">Fecha</p>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td><i class="mdi mdi-checkbox-blank-circle text-danger"></i> Pago Atrasado</td>
                                                            <td>
                                                                $44,584
                                                                <p class="m-0 text-muted font-14">Monto</p>
                                                            </td>
                                                            <td>
                                                                7/11/2016
                                                                <p class="m-0 text-muted font-14">Fecha</p>
                                                            </td>
                                                        </tr>

                                                    </tbody>
                                                </table>
                                            </div>
                                            <hr>
                                            <h4 class="mt-0 header-title">PROGRESO</h4>
                                            <div class="container">
                                                <div class="progress m-b-30">
                                                <div class="progress-bar" role="progressbar" style="width: 75%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">75%</div>
                                                </div>
                                            </div>
                          
                                    </div>
                                </div>
                            </div>

                        </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->




        <!-- Modal para editar usuario -->
        <div class="modal fade modal-editar" tabindex="-1" role="dialog" aria-labelledby="editarCliente" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title mt-0" id="editarCliente"><i class="fa fa-spin fa-cog"></i> Editar cliente</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card m-b-20">
                                        <div class="card-body">
                                            <form action="" method="POST">
                                                <input id="hidden" name="hidden" type="hidden">
                                                <div class="form-group row">
                                                    <label for="nya" class="col-sm-2 col-form-label">Nombre y apellido</label>
                                                    <div class="col-sm-10">
                                                        <input class="form-control" type="text" name="nya" value="" id="nya" placeholder="Nombre y apellido" required>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="dni" class="col-sm-2 col-form-label">Documento</label>
                                                    <div class="col-sm-10">
                                                        <input class="form-control" type="text" name="dni" value="" id="dni" placeholder="Documento de identidad" required>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="email" class="col-sm-2 col-form-label">Email</label>
                                                    <div class="col-sm-10">
                                                        <input class="form-control" type="email" name="email" id="email" placeholder="Email" required>
                                                    </div>
                                                </div>
                                                <!-- <div class="form-group row">
                                                    <label for="pass" class="col-sm-2 col-form-label">Contraseña</label>
                                                    <div class="col-sm-10">
                                                        <input class="form-control" type="password" name="pass" placeholder="Contrase&ntilde;a" id="pass">
                                                    </div>
                                                </div> -->
                                                <div class="form-group row">
                                                    <label for="telefono" class="col-sm-2 col-form-label">Teléfono (opcional)</label>
                                                    <div class="col-sm-10">
                                                        <input class="form-control" type="number" name="telefono" value="" id="telefono" placeholder="Teléfono">
                                                    </div>
                                                </div>

                                                <div class="row form-group">
                                                    <div class="col-md-10"></div>
                                                    <div class="float-right col-md-2">
                                                    <button type="submit" name="submit" style="text-align:right !important;" class="btn btn-primary float-right">Editar</button>
                                                    </div>
                                                </div>
                                                
                                            </form>
                                        </div>
                                    </div>
                                </div> <!-- end col -->
                            </div>
                        </div>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->








    <div class="row">
        <div class="col-12">
            <div class="card m-b-30">
                <div class="card-body">
                    <h4 class="mt-0 header-title">Listado de clientes</h4>
                    <table class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;" id="datatable">
                        <thead>
                            <tr>
                                <th>Nombre y Apllido</th>
                                <th>Email</th>
                                <th>Tel</th>
                                <th>Dni</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
$( document ).ready(function() {

    //listar_clientes();
   
    initDatatable(); //Inicia tabla

});


    function initDatatable(){
            $('#datatable').DataTable().destroy()
            var table = $('#datatable').DataTable({
                "dom": "<'row'<'col-sm-12 col-md-6'B><'col-sm-12 col-md-6'f>><'row'<'col-sm-12't>><'row'<'col-sm-12 col-md-5'l><'col-sm-12 col-md-7'p>>",
                "processing": true,
                "serverSide": true,
                "ordering": false,
                "language": {
                        "url": "https://cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
                },
                "ajax":{
                    'url': "../admin/phpfiles/listar.clientes.php",
                    'type': "post", // method  , by default get
                    complete: function() {
                        console.log("exitoso");
                    },
                    error: function() { // error handling
                        $(".employee-grid-error").html("");
                        $("#tabla-clientes").html('<tbody class="tabla-clientes-error"><tr><th colspan="3">No se encontraron resultados</th></tr></tbody>');
                        $("#tabla-clientes_processing").css("display", "none");

                    }
                }
            });
    }

    function editar_cliente(id) {
        $.ajax({
            url: "../admin/phpfiles/buscar.cliente.php",
            type: "GET",
            data: {'id':id },
            success: function (data) {
                datos = JSON.parse(data);
                $('#hidden').val(datos.id);
                $('#nya').val(datos.nombre);
                $('#dni').val(datos.dni);
                $('#email').val(datos.email);
                $('#telefono').val(datos.telefono);
            }
        });
    }
</script>