<?php

include_once('../mode/block.class.admin.php');

$listaLote = new Admin();

if(isset($_POST['submit'])){
    $ln = $_POST['ln'];
    $mz = $_POST['mz'];
    $barrio = $_POST['barrio'];
    $estado = 0;

    $msg = $listaLote->setNuevoLote($ln, $mz, $barrio, $estado);
}

?>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card m-b-20">
                <div class="card-body">
                    <h4 class="mt-0 header-title">Nuevo lote</h4>
                    <form action="" method="POST">
                        <div class="form-group row">
                            <label for="ln" class="col-sm-2 col-form-label">Numero de lote</label>
                            <div class="col-sm-10">
                                <input class="form-control" type="number" name="ln" value="" id="nl" placeholder="Numero de lote" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="mz" class="col-sm-2 col-form-label">Manzana</label>
                            <div class="col-sm-10">
                                <input class="form-control" type="number" name="mz" id="mz" placeholder="Manzana" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label" for="place" >Barrio</label>
                            <div class="col-sm-10  input-group">
                                <select id="barrio" name="barrio" class="form-control select2" required>
                                    <option value="Sol de Domselaar">Sol de Domselaar</option>
                                    <option value="Oasis">Oasis</option>
                                    <option value="Los Tilos">Los Tilos</option>
                                </select>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-10"></div>
                            <div class="float-right col-md-2">
                              <button type="submit" name="submit" style="text-align:right !important;" class="btn btn-primary float-right">Agregar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div> <!-- end col -->
    </div>

<script src="../assets/plugins/sweet-alert2/sweetalert2.min.js"></script>
<script src="../assets/pages/sweet-alert.init.js"></script>
<script>
    $( document ).ready(function() {

        <?php if(isset($_POST['submit'])){ ?>
            fireSuccess('<?php echo$msg?>')
        <?php }?>

});
</script>
