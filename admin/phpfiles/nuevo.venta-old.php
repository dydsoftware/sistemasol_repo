<?php
//en compra lote actualizar la no disponibilidad
include_once('../mode/block.class.admin.php');

$blok = new Admin();

if(isset($_POST['submit'])){
  $bclienteid = $_POST['nombre'];
  $idloteb = $_POST['lote'];
  $bmoneda = $_POST['mcambio'];
  $plan = $_POST['plan'];
  $adelanto = $_POST['adelanto'];
  $mtotal = $_POST['total'];
  $vcuota = $_POST['vcuota'];
  $vcuotapesos = $_POST['vcuotapesos'];
  $cambio = $_POST['cambio'];
  $bfechaalta = date('Y-m-d');
  $bfechavenceplan  = date('Y-m-d');//sumar la cantidad de cuotas

  $blok->getAltaCompraLote($bclienteid, $idloteb, $bmoneda, $plan, $adelanto, $mtotal, $vcuota, $vcuotapesos, $cambio, $bfechaalta, $bfechavenceplan);
}

$mLote = $blok->getListaLoteDisponibles();

$datos = $blok->getAutocompletar(); // Lista de clientes

?>
<h5 class="mb-4">Nueva Venta</h5>
<form action="" method="POST">
<div class="mb-3">
<div class="row g-3">

<div class="col-md-5">
<label for="cliente" class="form-label">Cliente</label>

<span class="input-group-text" id="nombre">ID</span>

<!-- Input autorrelleno -->

 <input type="text" class="form-control" name="nombre" id="cliente" list="miLista" autocomplete="off">

<!-- <input id="clientes-list" class="clientes-list form-control form-select" style="padding:0px" name="clientes-list"> -->

<datalist id="miLista">
<?php foreach($datos as $c){ ?>
<option data-id="<?=$c['id'];?>" value="<?=$c['id'];?>"><?=$c['nya'];?> - <?=$c['email'];?></option>
<?php } ?>
</datalist>


</div>

<div class="col-md-4">
<label for="lote" class="form-label">Lote</label>
<select class="form-select" name="lote" id="lote" required>
<option selected disabled>Buscar lote</option>
<?php

if($mLote){
  foreach($mLote as $rowInfo){
?>
<option value="<?=$rowInfo['idlote'];?>">
    L<?=$rowInfo['lotenumero'];?> - MZ <?=$rowInfo['manzana'];?> - <?=$rowInfo['barrio'];?>
</option>

<?php
  }
} else {
    echo 'No hay datos';
}
?>
</select>
</div>

<div class="col-md-3">
<label for="mcambio" class="form-label">Cambio</label>
<select class="form-select" name="mcambio" id="mcambio" required>
<option selected disabled>Moneda</option>
<option value="$">$ - Peso</option>
<option value="U$S">U$S - D&oacute;lar</option>
</select>
</div>

<!-- Calculo de pago -->

<div class="col-md-2">
<label>Valor Lote</label>
<input type="number" name="" class="form-control" id="valorL" step="0.001" oninput="calcular()">
</div>

<div class="col-md-2">
<label>Adelanto</label>
<input type="number" name="adelanto" class="form-control" id="adelanto" step="0.001" oninput="calcular()">
</div>

<div class="col-md-1">
<label>Cuotas</label>
<input type="number" name="plan" class="form-control" id="cuotas" step="1" oninput="calcular()">
</div>

<div class="col-md-2">
<label>Precio por cuota</label>
<input type="number" name="vcuota" class="form-control" id="totalCuo" step="0.001" oninput="calcular()">
</div>

<div class="col-md-2">
<label>Cambio actual</label>
<input type="number" name="cambio" class="form-control" id="cambio" step="0.001" oninput="calcular()">
</div>

<div class="col-md-3">
<label>Total en pesos</label>
<input type="number" name="vcuotapesos" class="form-control" id="convPeso" step="0.001" oninput="calcular()"> 
</div>

<div class="col-md-3">
<label>Total </label>
<input type="number" name="total" class="form-control" id="total" step="0.001" oninput="calcular()">
</div>

</div>



</div>

<button type="submit" name="submit" class="btn btn-primary">Agregar</button>
</form>

<script>
$( document ).ready(function() {



});

/*$( "datalist" ).change(function() {
  alert( "Handler for .change() called." );
});*/

</script>

<!-- <script src="../asset/plugins/select2/select2.min.js" type="text/javascript"></script>  -->

<script>
$( document ).ready(function() {
  
  //listar_clientes_select()
  $("#clientes-list").select2({
    placeholder: 'Busque un cliente',
    minimumInputLength: 1,
    allowClear: true,
    ajax: {
        url: "phpfiles/listar.clientes.select.php",
        dataType: 'json',
        data: function(term,page) {
            return {
                cliente: term.nombre, // search term
            };
        },
        results: function(data,page) {
            alert(JSON.stringify(data))
            return data;
        },
        cache: true
    }
  }).on('select2-open', function() {
  $(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
  });

});
</script>