<?php
//hacer funcion de calculo en progressbar
include_once('../mode/block.class.admin.php');
$blok = new Admin();

// datos de compra del usuario
$row = $blok->getSelectCompraLote($_GET['id']);
?>

<div class="pb-5">
<div class="text-center">
<img src="../asset/<?=$row['avatar'];?>" class="rounded-circle border-white" alt="avatar" width="120">
<h4 class="mt-2"><?=$row['nya'];?></h4>
<div class="small text-muted">
<span><i class="fas fa-paper-plane"></i> <?=$row['email'];?></span>
<span class="mx-2"><i class="fas fa-phone-alt"></i> <?=$row['telefono'];?></span>
<span><i class="fas fa-map-marker-alt"></i> <?=$row['barrio'];?></span>
</div>


<div class="mt-4">
<a href="./?page=nuevo.factura&id=<?=$row['id'];?>" class="btn btn-light btn-sm"><i class="fas fa-pencil-alt"></i> Crear Factura</a>
</div>

</div>



</div>



<div class="row">

<div class="col-md-7 mb-4">
<div class="card">
<div class="card-header">Detalles de compra - Lote <?=$row['lotenumero'];?> - MZ <?=$row['manzana'];?><?=$row['mz'];?></div>
<div class="card-body">
<ul class="list-group list-group-flush">
<li class="list-group-item d-flex justify-content-between align-items-center">
Valor del lote:
<span>U$S <?=$row['mtotal'];?></span>
</li>
<li class="list-group-item d-flex justify-content-between align-items-center">
Adelanto:
<span>U$S <?=$row['adelanto'];?></span>
</li>
<li class="list-group-item d-flex justify-content-between align-items-center">
<?php
$valor = $row['mtotal'];
$adelanto = $row['adelanto'];
$total = $valor - $adelanto;
?>
A pagar:
<span>U$S <?=$total;?>.00</span>
</li>
<li class="list-group-item d-flex justify-content-between align-items-center">
Plan:
<span><?=$row['plan'];?> Cuotas</span>
</li>
<li class="list-group-item d-flex justify-content-between align-items-center">
Valor de cuota:
<span>U$S <?=$row['vcuota'];?></span>
</li>
<li class="list-group-item d-flex justify-content-between align-items-center">
Cuota en Pesos:
<span>$ <?=$row['vcuotapesos'];?></span>
</li>
</ul>
</div>
</div>
</div>

<div class="col-md-5 mb-4">
<div class="card">
<div class="card-body">
<p class="card-text">Cuotas 9/<?=$row['plan'];?></p>
<div class="progress" style="height: 6px;">
<div class="progress-bar" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
</div>
</div>
</div>
</div>

</div>