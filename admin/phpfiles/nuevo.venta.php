<?php

include_once('../mode/block.class.admin.php');

$venta = new Admin();

if(isset($_POST['submit'])){

    $data = array(
            $_POST['id_cliente'],
            $_POST['id_lote'],
            $_POST['id_vendedor'],
            $_POST['moneda'],
            $_POST['telefono'],
            $_POST['alicuota'],
            $_POST['cuota_refuerzo'],
            $_POST['concepto'],
            3,
            date('Y-m-d'),
            $_POST['dni']
            );

    $msg = $venta->setNuevaFactura($data);
 
}

?>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
      
            <div class="card m-b-20">
                <div class="card-body container-fluid">

                <div class="row justify-content-center mt-0">
                    <div class="col-11 col-sm-9 col-md-7 col-lg-6 p-0 mt-3 mb-2">
                        <div class="card px-0 pt-4 pb-0 mt-3 mb-3">
                        <div class="row">
                        <div class="col-md-12 mx-0">
                            <h4 class="mt-0 header-title">Crear Nueva Venta</h4>
                            <hr>
                            <form action="" method="POST">
                                <div class="form-group">
                                    <label>Cliente</label>
                                    <select class="form-control" name="cliente" id="lista-clientes">
                                        <option selected="selected">Cliente 1</option>
                                        <option>white</option>
                                        <option>purple</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Lote</label>
                                    <select class="form-control" name="lote" id="lista-lotes">
                                        <option selected="selected">Lote1</option>
                                        <option>white</option>
                                        <option>purple</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Vendedor</label>
                                    <select class="form-control" name="vendedor" id="lista-vendedores">
                                        <option selected="selected">Vendedor1</option>
                                        <option>white</option>
                                        <option>purple</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Moneda</label>
                                    <select class="form-control" name="moneda" id="lista-monedas">
                                        <option selected="selected">Pesos</option>
                                        <option>white</option>
                                        <option>purple</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Venta financiada</label> <br>
                                    <input class="switch-active" data-id="venta_financiada" type="checkbox" id="switch6" switch="primary" checked/>
                                    <label for="switch6" data-on-label="Si"
                                           data-off-label="No"></label>
                                </div>
                                <div class="form-group row" hidden="true" id="div_monto_venta">
                                    <label for="monto_venta" class="col-sm-8 col-form-label">Monto total <small>Caso directa</small>
                                    </label>
                                    <div class="col-sm-12">
                                        <input class="form-control" type="number" value="" id="monto_venta">
                                    </div>
                                </div>
                                <div class="form-group" id="div_alicuota">
                                    <label>Alicuota</label> <br>
                                    <input class="switch-active" type="checkbox" data-id="alicuota" id="alicuota" switch="success" checked/>
                                    <label for="alicuota" data-on-label="Si"
                                           data-off-label="No"></label>
                                </div>
                                <div class="form-group row" id="div_frecuencia">
                                    <label for="frecuencia" class="col-sm-2 col-form-label">Frecuencia</label>
                                    <div class="col-sm-12">
                                        <input class="form-control" type="number" value="" id="frecuencia">
                                    </div>
                                </div>
                                <div class="form-group row" id="div_porcentaje">
                                    <label for="porcentaje" class="col-sm-2 col-form-label">Porcentaje</label>
                                    <div class="col-sm-12">
                                        <input class="form-control" type="number" value="" id="porcentaje">
                                    </div>
                                </div>
                                <div class="form-group row" id="div_concepto">
                                    <label class="col-sm-12 col-form-label">Concepto</label>
                                    <div class="col-sm-4">
                                        <select class="form-control" name="concepto" id="concepto">
                                            <option value="extra" selected="selected">Extra</option>
                                            <option value="regular">Regular</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="input-daterange input-group" id="date-range">
                                            <input type="text" placeholder="Fecha inicio" class="form-control" name="start" />
                                            <input type="text" placeholder="Fecha fin" class="form-control" name="end" />
                                        </div>
                                    </div>
                                    <div class="form-check form-check-inline ml-5">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                                        <label class="form-check-label" for="inlineCheckbox1">Ene</label>
                                    </div>
                                    <div class="form-check form-check-inline  ml-5">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2">
                                        <label class="form-check-label" for="inlineCheckbox2">Feb</label>
                                    </div>
                                    <div class="form-check form-check-inline  ml-5">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox3" value="option3">
                                        <label class="form-check-label" for="inlineCheckbox3">Mar</label>
                                    </div>
                                    <div class="form-check form-check-inline  ml-5">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox4" value="option4">
                                        <label class="form-check-label" for="inlineCheckbox4">Abr</label>
                                    </div>
                                    <div class="form-check form-check-inline  ml-5">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox5" value="option5">
                                        <label class="form-check-label" for="inlineCheckbox5">May</label>
                                    </div>
                                    <div class="form-check form-check-inline  ml-5">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox6" value="option6">
                                        <label class="form-check-label" for="inlineCheckbox6">Jun</label>
                                    </div>
                                    <div class="col-12"></div>
                                    <div class="form-check form-check-inline  ml-5">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox7" value="option7">
                                        <label class="form-check-label" for="inlineCheckbox7">Jul</label>
                                    </div>
                                    <div class="form-check form-check-inline  ml-5">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox8" value="option8">
                                        <label class="form-check-label" for="inlineCheckbox8">Ago</label>
                                    </div>
                                    <div class="form-check form-check-inline  ml-5">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox9" value="option9">
                                        <label class="form-check-label" for="inlineCheckbox9">Sep</label>
                                    </div>
                                    <div class="form-check form-check-inline  ml-5">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox10" value="option10">
                                        <label class="form-check-label" for="inlineCheckbox10">Oct</label>
                                    </div>
                                    <div class="form-check form-check-inline  ml-5">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox12" value="option11">
                                        <label class="form-check-label" for="inlineCheckbox11">Nov</label>
                                    </div>
                                    <div class="form-check form-check-inline  ml-5">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox12" value="option12">
                                        <label class="form-check-label" for="inlineCheckbox12">Dic</label>
                                    </div>
                                </div>
                                <div class="form-group" id="div_cuota_refuerzo">
                                    <label>Cuota Refuerzo</label> <br>
                                    <input class="switch-active" data-id="cuota_refuerzo" type="checkbox" id="switch8" switch="info" checked/>
                                    <label for="switch8" data-on-label="Si"
                                           data-off-label="No"></label>
                                </div>
                                <div class="form-group row" id="div_mes_refuerzo">
                                    <div class="col-sm-6">
                                        <select class="form-control" name="mes_refuerzo" id="mes_refuerzo">
                                            <option value="1" selected="selected">Enero</option>
                                            <option value="2">Febrero</option>
                                            <option value="3">Marzo</option>
                                            <option value="4">Abril</option>
                                            <option value="5">Mayo</option>
                                            <option value="6">Junio</option>
                                            <option value="7">Julio</option>
                                            <option value="8">Agosto</option>
                                            <option value="9">Septiembre</option>
                                            <option value="10">Octubre</option>
                                            <option value="11">Noviembre</option>
                                            <option value="12">Diciembre</option>                        
                                        </select>
                                    </div>
                                    <div class="col-sm-6" id="div_monto_mes_refuerzo">
                                        <div class="input input-group" id="monto_mes_refuerzo">
                                            <input type="text" class="form-control" placeholder="Monto cuota refuerzo" name="monto_mes_refuerzo" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-10"></div>
                                        <div class="float-right col-md-2">
                                        <button type="submit" name="submit" style="text-align:right !important;" class="btn btn-primary float-right">Agregar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        </div>
                    </div>
                </div>
            </div>

                   
                </div>
            </div>  
        </div> <!-- end col -->
    </div>

</div>

<script src="../assets/plugins/sweet-alert2/sweetalert2.min.js"></script>
<script src="../assets/pages/sweet-alert.init.js"></script>
<script>
    $( document ).ready(function() {
        <?php if(isset($_POST['submit'])){//si se carga una venta se dispara alerta success ?>
            fireSuccess('<?php echo$msg?>')
        <?php }?>
        
    });
    
    $( document ).ready(function() {
        
        listar_clientes_select()//crea jQuery para el selec2
        //$.fn.select2.defaults.set('language', 'es');
        $(".switch-active").change(function() {
            var value = this.checked ? 0 : 1;
            if($(this).attr('data-id') == 'venta_financiada'){
                if(value == 1) {
                    $('#div_monto_venta').attr("hidden",false);
                    $('#div_concepto').attr("hidden",true);
                    $('#div_alicuota').attr("hidden",true);
                    $('#div_frecuencia').attr("hidden",true);
                    $('#div_porcentaje').attr("hidden",true);
                    $('#div_cuota_refuerzo').attr("hidden",true);
                    $('#div_mes_refuerzo').attr("hidden",true);
                    $('#div_monto_mes_refuerzo').attr("hidden",true);
                }else {
                    $('#div_monto_venta').attr("hidden",true);
                    $('#div_concepto').attr("hidden",false);
                    $('#div_alicuota').attr("hidden",false);
                    $('#div_frecuencia').attr("hidden",false);
                    $('#div_porcentaje').attr("hidden",false);
                    $('#div_cuota_refuerzo').attr("hidden",false);
                    $('#div_mes_refuerzo').attr("hidden",false);
                    $('#div_monto_mes_refuerzo').attr("hidden",false);
                }
            }else if($(this).attr('data-id') == 'alicuota'){
                if(value == 1) {
                    $('#div_frecuencia').attr("hidden",true);
                    $('#div_porcentaje').attr("hidden",true);
                }else {
                    $('#div_frecuencia').attr("hidden",false);
                    $('#div_porcentaje').attr("hidden",false);
                }
            }else {
                if(value ==1 ) {
                    $('#div_mes_refuerzo').attr("hidden",true);
                    $('#div_monto_mes_refuerzo').attr("hidden",true);
                }else {
                    $('#div_mes_refuerzo').attr("hidden",false);
                    $('#div_monto_mes_refuerzo').attr("hidden",false);
                }
            }
        });
        
    });
    
    //$.fn.select2.defaults.set('language', 'es');
    
    
    
    /*$("#lista-clientes").select2({
        tags: true,
        placeholder: 'Buscar cliente',
        minimumInputLength: 1,
        allowClear: true,
    });*/
    
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/i18n/es.js"></script>