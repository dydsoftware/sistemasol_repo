<?php
include_once('../mode/block.class.admin.php');
$blok = new Admin();

$lista = $blok->getListaVendedoresFiltro(array());

?>
<div class="container">
            <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title mt-0">Detalle del Vendedor</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <!-- <div class="col-6">
                                    <p>Hola</p>
                                </div> -->
                                <div class="col-12">
                                <div class="card-body">
                            <div class="col-md-8">
                            <h4 class="mt-0 header-title">Ventas Realizadas</h4>
                            </div>

                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs nav-tabs-custom nav-justified" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active show" data-toggle="tab" href="#home2" role="tab" aria-selected="false">Lote 1 - Sol de Domselaar</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#profile2" role="tab" aria-selected="false">Lote 2 - Oasis</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#messages2" role="tab" aria-selected="true">Lote 3 - Los Tilos</a>
                                </li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane p-3 active show" id="home2" role="tabpanel">
                                <div class="card m-b-30">
                                      

                                <div class="table-responsive">
                                                <table class="table m-t-20 mb-0 table-vertical">

                                                    <tbody>

                                                        <tr>
                                                            <td><i class="mdi mdi-checkbox-blank-circle text-success"></i> 1%
                                                                <p class="m-0 text-muted font-14"> Comisión</p>
                                                            </td>
                                                            <td>
                                                                $8,541
                                                                <p class="m-0 text-muted font-14">Monto</p>
                                                            </td>
                                                            <td>
                                                                10/11/2020
                                                                <p class="m-0 text-muted font-14">Fecha</p>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td><i class="mdi mdi-checkbox-blank-circle text-success"></i> 1.5%
                                                                <p class="m-0 text-muted font-14"> Comisión</p>
                                                            </td>
                                                            <td>
                                                                $12,000
                                                                <p class="m-0 text-muted font-14">Monto</p>
                                                            </td>
                                                            <td>
                                                                8/11/2020
                                                                <p class="m-0 text-muted font-14">Fecha</p>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td><i class="mdi mdi-checkbox-blank-circle text-success"></i> 2%
                                                                <p class="m-0 text-muted font-14"> Comisión</p>
                                                            </td>
                                                            <td>
                                                                $44,584
                                                                <p class="m-0 text-muted font-14">Monto</p>
                                                            </td>
                                                            <td>
                                                                7/11/2016
                                                                <p class="m-0 text-muted font-14">Fecha</p>
                                                            </td>
                                                        </tr>

                                                    </tbody>
                                                </table>
                                            </div>
                                            <hr>
                                    
                                    </div>
                                </div>
                                <div class="tab-pane p-3" id="profile2" role="tabpanel">
                                    <div class="card m-b-30">
                                       

                                    <div class="table-responsive">
                                                <table class="table m-t-20 mb-0 table-vertical">

                                                    <tbody>

                                                        <tr>
                                                            <td><i class="mdi mdi-checkbox-blank-circle text-success"></i> 1%
                                                                <p class="m-0 text-muted font-14"> Comisión</p>
                                                            </td>
                                                            <td>
                                                                $8,541
                                                                <p class="m-0 text-muted font-14">Monto</p>
                                                            </td>
                                                            <td>
                                                                10/11/2020
                                                                <p class="m-0 text-muted font-14">Fecha</p>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td><i class="mdi mdi-checkbox-blank-circle text-success"></i> 1.5%
                                                                <p class="m-0 text-muted font-14"> Comisión</p>
                                                            </td>
                                                            <td>
                                                                $12,000
                                                                <p class="m-0 text-muted font-14">Monto</p>
                                                            </td>
                                                            <td>
                                                                8/11/2020
                                                                <p class="m-0 text-muted font-14">Fecha</p>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td><i class="mdi mdi-checkbox-blank-circle text-success"></i> 2%
                                                                <p class="m-0 text-muted font-14"> Comisión</p>
                                                            </td>
                                                            <td>
                                                                $44,584
                                                                <p class="m-0 text-muted font-14">Monto</p>
                                                            </td>
                                                            <td>
                                                                7/11/2016
                                                                <p class="m-0 text-muted font-14">Fecha</p>
                                                            </td>
                                                        </tr>

                                                    </tbody>
                                                </table>
                                            </div>
                                            <hr>
                                    
                                    </div>
                                </div>
                                <div class="tab-pane p-3" id="messages2" role="tabpanel">
                                <div class="card m-b-30">
                                  
                                            <div class="table-responsive">
                                                <table class="table m-t-20 mb-0 table-vertical">

                                                    <tbody>

                                                        <tr>
                                                            <td><i class="mdi mdi-checkbox-blank-circle text-success"></i> 1%
                                                                <p class="m-0 text-muted font-14"> Comisión</p>
                                                            </td>
                                                            <td>
                                                                $8,541
                                                                <p class="m-0 text-muted font-14">Monto</p>
                                                            </td>
                                                            <td>
                                                                10/11/2020
                                                                <p class="m-0 text-muted font-14">Fecha</p>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td><i class="mdi mdi-checkbox-blank-circle text-success"></i> 1.5%
                                                                <p class="m-0 text-muted font-14"> Comisión</p>
                                                            </td>
                                                            <td>
                                                                $12,000
                                                                <p class="m-0 text-muted font-14">Monto</p>
                                                            </td>
                                                            <td>
                                                                8/11/2020
                                                                <p class="m-0 text-muted font-14">Fecha</p>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td><i class="mdi mdi-checkbox-blank-circle text-success"></i> 2%
                                                                <p class="m-0 text-muted font-14"> Comisión</p>
                                                            </td>
                                                            <td>
                                                                $44,584
                                                                <p class="m-0 text-muted font-14">Monto</p>
                                                            </td>
                                                            <td>
                                                                7/11/2016
                                                                <p class="m-0 text-muted font-14">Fecha</p>
                                                            </td>
                                                        </tr>

                                                    </tbody>
                                                </table>
                                            </div>
                                            <hr>
                          
                                    </div>
                                </div>
                            </div>

                        </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->


    <div class="row">
        <div class="col-12">
            <div class="card m-b-30">
                <div class="card-body">
                    <h4 class="mt-0 header-title">Listado de clientes</h4>
                    <hr>
                    <table class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;" id="tabla-vendedores">
                        <thead>
                            <tr>
                                <th>Nombre y Apllido</th>
                                <th>Dni</th>
                                <th>Email</th>
                                <th>Tel</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
$( document ).ready(function() {

    listar_vendedores();
    //initDatatable();
});

</script>