<?php

include_once('dbh.class.php');

class Admin extends Dbh {

    public function login(){
        print_r($_POST);
     
        if(isset($_POST['add'])){
            if(!empty($_POST['email']) && !empty($_POST['pass'])){
                $sql = "SELECT * FROM usuario WHERE email = :email AND pass = :pass";
                $stmt = $this->connect()->prepare($sql);
                $stmt->execute(array(':email' => $_POST['email'], ':pass' => $_POST['pass']));
                $fila = $stmt->fetch(PDO::FETCH_ASSOC);
                if($stmt->rowCount() > 0){
                    $_SESSION['logueado'] = "SI";
                    $_SESSION['usuario']  = $fila['nombre'];
                    $_SESSION['nivel'] = $fila['nivel'];
                    header('location: ../');
                } else {
                    echo 'Email or Password icorrect';
                }
            } else {
                echo 'Enter email user and password';
            }
        }
        
    }

    public function eliminarLote($id) {
        $sql = "DELETE FROM infolote WHERE idlote = ".$id;
        $stmt = $this->connect()->query($sql);
    }

    public function getUsuario($id){
        $sql = "SELECT * FROM usuario WHERE nombre = ?";
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute(array($id));
        
        $resultado = $stmt->fetch();
        return $resultado;
    }

    public function getCliente($id) {
        $sql = "SELECT * FROM usuario WHERE nivel=3 and id = ?";
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute(array($id));
        
        $resultado = $stmt->fetch();
        return json_encode($resultado);
    }

    public function updateCliente($data,$id) {
        $sql = "UPDATE usuario SET nombre = ?, email = ?, telefono = ?, dni = ?  WHERE id =".$id;
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute($data);
    }

    public function getListaLote(){
        $sql = "SELECT * FROM infolote";
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute();

        while($resultado = $stmt->fetchAll()){
            return $resultado;
        }
    }

    // Lista solo los lotes que no se vendieron
    public function getListaLoteDisponibles(){
        $sql = "SELECT * FROM infolote WHERE loteestado = 0";
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute();

        while($resultado = $stmt->fetchAll()){
            return $resultado;
        }
    }

    public function setNuevoLote($ln, $mz, $barrio, $estado){
        $sql = "INSERT INTO infolote (lotenumero, manzana, barrio, loteestado) VALUES (?, ?, ?, ?)";
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute([$ln, $mz, $barrio, $estado]);
        
        return 'Carga Exitosa';
    }

    public function setNuevoCliente($data){

        $sql = "INSERT INTO usuario (nombre, email, pass, avatar, telefono, nivel, fecha, dni) VALUES (?,?,?,?,?,?,?,?)";
        $stmt = $this->connect()->prepare($sql);
        $response = $stmt->execute($data);
        
        if($response)
            return 'Carga Exitosa';
            
    }

    public function getListaClientes(){

        $sql = "SELECT * FROM usuario WHERE nivel=3";
        $stmt = $this->connect()->query($sql)->fetchAll();

        return $stmt;
    }


    /*
    *
    *Vendedores
    *
    */

    //cargar
    public function setNuevoVendedor($data){

        $sql = "INSERT INTO vendedor (nombre, dni, email, avatar, telefono) VALUES (?,?,?,?,?)";
        $stmt = $this->connect()->prepare($sql);
        $response = $stmt->execute($data);
        print_r($response);
        if($response)
            return 'Carga Exitosa';
            
    }


    //listar
    public function getListaVendedoresFiltro($requestData){
        

        $sql = "SELECT id,nombre, email, telefono, dni FROM vendedor WHERE 1=1";
        $stmt = $this->connect()->query($sql)->fetchAll();
     
        $totalData = $this->connect()->query($sql)->rowCount();
        $totalFiltered = $totalData;  // when there is no search parameter then total number rows = total number filtered rows.

        if( !empty($requestData['search']['value']) ) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
            $sql.=" AND ( nombre LIKE '%".$requestData['search']['value']."%' ";    
            $sql.=" OR email LIKE '%".$requestData['search']['value']."%') ";
        }
        if( !empty($requestData['length']) ) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
            $sql.="  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
        }

        $stmt = $this->connect()->query($sql)->fetchAll();

        $data = array();
        $index = 1;
        foreach( $stmt as $row ) {  // preparing an array
            
            $nestedData=array(); 
            
            $nestedData[] = $row["nombre"];
            $nestedData[] = $row["dni"];
            $nestedData[] = $row["email"];
            $nestedData[] = $row["telefono"];
            $nestedData[] = '<div class="dropdown">
            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Acciones
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <!-- <a class="dropdown-item" href="#">Lotes</a>
              <a class="dropdown-item" href="#">Cuotas</a> -->
              <a class="dropdown-item" data-toggle="modal" data-target=".bs-example-modal-lg" href="#">Detalle</a>
            </div>
          </div>';
            
            $data[] = $nestedData;
        }



        $json_data = array(
                    "recordsTotal"    => intval( $totalData ),  // total number of records
                    "recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
                    "data"            => $data, // total data array
                    'test' => $sql   
                    );

        return json_encode($json_data);  // send data as json format

}

    /*
    *
    *Fin vendedores
    *
    */
   
    public function getListaLotesFiltro($requestData){

        $sql = "SELECT idlote,lotenumero, manzana, barrio, loteestado FROM infolote WHERE 1=1";
        $stmt = $this->connect()->query($sql)->fetchAll();
        
        $totalData = $this->connect()->query($sql)->rowCount();
        $totalFiltered = $totalData;  // when there is no search parameter then total number rows = total number filtered rows.

        if( !empty($requestData['search']['value']) ) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
            $sql.=" AND ( lotenumero LIKE '%".$requestData['search']['value']."%' ";    
            $sql.=" OR manzana LIKE '%".$requestData['search']['value']."%') ";
        }
        if( !empty($requestData['length']) ) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
            $sql.="  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
        }

        $stmt = $this->connect()->query($sql)->fetchAll();
        //$totalFiltered = $this->connect()->query($sql)->rowCount();
        $data = array();
        $index = 1;
        foreach( $stmt as $row ) {  // preparing an array
            
            $nestedData=array(); 
            
            $nestedData[] = $row["lotenumero"];
            $nestedData[] = $row["manzana"];
            $nestedData[] = $row["barrio"];
            $nestedData[] = $row["loteestado"];
            
            $nestedData[] = "<a onclick='eliminar_lote(".$row['idlote'].")'><i class=\"fa fa-trash\"></i></a><br>";
            
            $data[] = $nestedData;
        }


        $json_data = array(
                    "recordsTotal"    => intval( $totalData ),  // total number of records
                    "recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
                    "data"            => $data, // total data array
                    'test' => $sql   
                    );

        return json_encode($json_data);  // send data as json format

    }

    public function getListaClientesFiltro($requestData){
        

            $sql = "SELECT id,nombre, email, telefono, dni FROM usuario WHERE nivel = 3 AND 1=1";
            $stmt = $this->connect()->query($sql)->fetchAll();
         
            $totalData = $this->connect()->query($sql)->rowCount();
            $totalFiltered = $totalData;  // when there is no search parameter then total number rows = total number filtered rows.
    
            if( !empty($requestData['search']['value']) ) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
                $sql.=" AND ( nombre LIKE '%".$requestData['search']['value']."%' ";    
                $sql.=" OR email LIKE '%".$requestData['search']['value']."%') ";
            }
            if( !empty($requestData['length']) ) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
                $sql.="  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
            }
    
            $stmt = $this->connect()->query($sql)->fetchAll();
    
            $data = array();
            $index = 1;
            foreach( $stmt as $row ) {  // preparing an array
                
                $nestedData=array(); 
                
                $nestedData[] = $row["nombre"];
                $nestedData[] = $row["email"];
                $nestedData[] = $row["telefono"];
                $nestedData[] = $row["dni"];
                $nestedData[] = '<div class="dropdown">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Acciones
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                  <!-- <a class="dropdown-item" href="#">Lotes</a>
                  <a class="dropdown-item" href="#">Cuotas</a> -->
                  <a class="dropdown-item" data-toggle="modal" data-target=".bs-example-modal-lg" href="#">Detalle</a>
                  <a onclick="editar_cliente('.$row['id'].')" class="dropdown-item" data-toggle="modal" data-target=".modal-editar" href="#">Editar</a>
                </div>
              </div>';
                
                $data[] = $nestedData;
            }
    
    
    
            $json_data = array(
                        "recordsTotal"    => intval( $totalData ),  // total number of records
                        "recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
                        "data"            => $data, // total data array
                        'test' => $sql   
                        );
    
            return json_encode($json_data);  // send data as json format

    }


    // Este modulo es para guardar los datos en db.json
    public function getJson(){
        $sql = "SELECT * FROM cliente";
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute();
        $user = array();
        while($data = $stmt->fetch(PDO::FETCH_ASSOC)){
            $user[] = array(
                'id' => $data['id'],
                'nombre' => $data['nya'],
                'email' => $data['email']
            );
        }
        return $user;
    }

    public function getAutocompletar(){
        $sql = "SELECT * FROM cliente";
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute();
        $resultado = $stmt->fetchAll();
        return $resultado;
    }

    public function getBuscar($name){
        $sql = "SELECT nya FROM cliente WHERE nya LIKE :name";
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute([':name' => '%' . $name . '%']);
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $data;
    }

    public function getAltaCompraLote($bclienteid, $idloteb, $bmoneda, $plan, $adelanto, $mtotal, $vcuota, $vcuotapesos, $cambio, $bfechaalta, $bfechavenceplan){
        $sql = "INSERT INTO bcompralote (bclienteid, idloteb, bmoneda, plan, adelanto, mtotal, vcuota, vcuotapesos, cambio, bfechaalta, bfechavenceplan) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute([$bclienteid, $idloteb, $bmoneda, $plan, $adelanto, $mtotal, $vcuota, $vcuotapesos, $cambio, $bfechaalta, $bfechavenceplan]);

        echo '<p>El cliente fue agregado!</p>';
    }

    public function getListaBarrios(){
        // listar todos los usuarios que compraron en el barrio Sol de Domselaar, Oasis y Los Tilos
        $sql = "SELECT * FROM cliente INNER JOIN bcompralote ON bcompralote.bclienteid = cliente.id INNER JOIN infolote ON infolote.idlote = bcompralote.idloteb";
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute();

        $resultado = $stmt->fetchAll();
        return $resultado;
    }

    public function getSelectCompraLote($id){
        $sql = "SELECT * FROM cliente INNER JOIN bcompralote ON bcompralote.bclienteid = cliente.id INNER JOIN infolote ON infolote.idlote = bcompralote.idloteb WHERE idb = ?";
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute(array($id));

        $resultado = $stmt->fetch();
        return $resultado;
    }
}

?>