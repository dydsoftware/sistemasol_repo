<?php

include_once('dbh.class.php');

class Usuario extends Dbh {

    public function login(){
        if(isset($_POST['add'])){
            if(!empty($_POST['email']) && !empty($_POST['pass'])){
                $sql = "SELECT * FROM cliente WHERE email = :email AND pass = :pass";
                $stmt = $this->connect()->prepare($sql);
                $stmt->execute(array(':email' => $_POST['email'], ':pass' => $_POST['pass']));
                $fila = $stmt->fetch(PDO::FETCH_ASSOC);

                if($stmt->rowCount() > 0){
                    $_SESSION['logueado'] = "SI";
                    $_SESSION['usuario']  = $fila['nya'];
                    header('location: ../');
                } else {
                    echo 'Email or Password icorrect';
                }
            } else {
                echo 'Enter email user and password';
            }
        }
        
    }

    // Datos del usuario
    public function getUser($id){
        $sql = "SELECT * FROM cliente WHERE nya = ?";
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute(array($id));

        $resultado = $stmt->fetch();
        return $resultado;
    }

    // Lista las facturas de un usuario
    public function getFacturas($id){
        $sql = "SELECT * FROM fexpensas INNER JOIN cliente ON cliente.id = fexpensas.clientid WHERE clientid = ? ORDER BY eid DESC LIMIT 5";
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute(array($id));

        while($resultado = $stmt->fetchAll()){
            return $resultado;
        }
    }

    // Muestra una factura del usuario
    public function getSelectFactura($id){
        $sql = "SELECT * FROM fexpensas INNER JOIN cliente ON cliente.id = fexpensas.clientid WHERE eid = ?";
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute(array($id));

        $resultado = $stmt->fetch();
        return $resultado;
    }

    public function getDeuda($id){
        $sql = "SELECT *, SUM(total) as total FROM fexpensas WHERE eestado = 0 AND clientid = ?";
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute(array($id));

        $resultado = $stmt->fetch();
        return $resultado;
    }
}

class Notificaciones extends Dbh {
    public function getNotificaciones(){
        $sql = "SELECT * FROM notas";
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute();

        while($resultado = $stmt->fetchAll()){
            return $resultado;
        }
    }
}

class Lote extends Dbh {
    public function getUser($id){
        $sql = "SELECT * FROM cliente WHERE id = ?";
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute(array($id));

        $resultado = $stmt->fetch();
        return $resultado;
    }

    // lista las facturas de lote de un usuario
    public function getFacturasLote($id){
        $sql = "SELECT * FROM fclote INNER JOIN cliente ON cliente.id = fclote.clientid WHERE clientid = ? ORDER BY idf DESC LIMIT 5";
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute(array($id));

        while($resultado = $stmt->fetchAll()){
            return $resultado;
        }
    }

    // Total de cuotas pagas por lote
    public function getTotalCuotaLote($id){
        $sql = "SELECT *, SUM(total) as total FROM fclote INNER JOIN cliente ON cliente.id = fclote.clientid WHERE festado = 1 AND clientid = ?";
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute(array($id));

        $resultado = $stmt->fetch();
        return $resultado;
    }

    // Boleta de compra
    public function getInfoLote($id){
        $sql = "SELECT * FROM cliente INNER JOIN bcompralote ON bcompralote.bclienteid = cliente.id INNER JOIN infolote ON infolote.idlote = bcompralote.idloteb WHERE id = ?";
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute(array($id));

        while($resultado = $stmt->fetchAll()){
            return $resultado;
        }
    }
}

/*
 * Class Cliente
 * @return	bool
 */
class Cliente extends Dbh {
    public function getCliente($id){
        $sql = "SELECT * FROM cliente WHERE id = ?";
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute(array($id));

        $resultado = $stmt->fetch();
        return $resultado;
    }

    // lista las facturas de lote de un usuario
    public function getFacturasLote($id){
        $sql = "SELECT * FROM fclote INNER JOIN cliente ON cliente.id = fclote.clientid WHERE clientid = ? ORDER BY idf DESC LIMIT 5";
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute(array($id));

        while($resultado = $stmt->fetchAll()){
            return $resultado;
        }
    }

    // Total de cuotas pagas por lote
    public function getTotalCuotaLote($id){
        $sql = "SELECT *, SUM(total) as total FROM fclote INNER JOIN cliente ON cliente.id = fclote.clientid WHERE festado = 1 AND clientid = ?";
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute(array($id));

        $resultado = $stmt->fetch();
        return $resultado;
    }

    // Boleta de compra
    public function getInfoLote($id){
        $sql = "SELECT * FROM cliente INNER JOIN bcompralote ON bcompralote.bclienteid = cliente.id INNER JOIN infolote ON infolote.idlote = bcompralote.idloteb WHERE id = ?";
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute(array($id));

        while($resultado = $stmt->fetchAll()){
            return $resultado;
        }
    }
}

?>