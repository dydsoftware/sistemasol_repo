const d = document;

function calcular() {
    let lote = parseInt(d.getElementById("valorL").value) || 0,
        adelanto = parseInt(d.getElementById("adelanto").value) || 0,
        nCuota = parseInt(d.getElementById("cuotas").value) || 0,
        cambio = parseInt(d.getElementById("cambio").value) || 0;

    parseInt(d.getElementById("totalCuo").value) || 0;
    parseInt(d.getElementById("total").value) || 0;

    d.getElementById("total").value = (lote - adelanto).toFixed(2);
    d.getElementById("totalCuo").value = ((lote - adelanto) / nCuota).toFixed(2);
    d.getElementById("convPeso").value = (((lote - adelanto) / nCuota) * cambio).toFixed(2);
};

// funcion autocompletar

function search(name) {
    console.log(name);
    fetchSearchData(name);
}

function fetchSearchData(name) {
    fetch('../admin/phpfiles/buscar.php', {
            method: 'POST',
            body: new URLSearchParams('nya=' + name)
        })
        .then(res => res.json())
        .then(res => viewSearchResoult(res))
        .catch(e => console.error('Error: ' + e))
}

function viewSearchResoult(data) {
    const dataViewer = document.getElementById('dataViewer');

    dataViewer.innerHTML = '';

    for (let i = 0; i < data.length; i++) {
        const li = document.createElement('li');
        li.innerHTML = data[i]['nya'];
        dataViewer.appendChild(li);
    }
}

function listar_clientes() {
    var dataTable = $('#tabla-clientes').DataTable({
        "columnDefs": [
            { "width": "5%", "targets": 0 },
            { "width": "40%", "targets": 1 },
            { "width": "40%", "targets": 2 }
        ],
        "processing": true,
        paging: false,
        bSortable: true,
        "bInfo": false,
        "bLengthChange": false,
        aaSorting: [
            [1, 'asc']
        ],

        pageLength: 100,

        "serverSide": true,
        "language": {
            "sSearch": "Buscar: ",
            "processing": "Procesando...",
            "paginate": {
                "first": "Primera",
                "last": "Ultima",
                "next": "Siguiente",
                "previous": "Anterior"
            },

            "sZeroRecords": "No se encontraron resultados"
        },
        "ajax": {
            url: "../admin/phpfiles/listar.clientes.php", // json datasource
            type: "post", // method  , by default get
            error: function() { // error handling
                $(".employee-grid-error").html("");
                $("#tabla-clientes").html('<tbody class="tabla-clientes-error"><tr><th colspan="3">No se encontraron resultados</th></tr></tbody>');
                $("#tabla-clientes_processing").css("display", "none");

            }
        }
    });
}


function listar_clientes_select() {

    /*$("#clientes-list").select2({

        placeholder: 'Busque un cliente',
        minimumInputLength: 1,
        allowClear: true,
        ajax: {
            url: "../admin/phpfiles/listar.clientes.select.php",
            dataType: 'json',
            data: function(term) {
                return {
                    cliente: term, // search term
                };
            },
            formatResult: function(item) { return item.nombre; },
            formatSelection: function(item) { return item.nombre; },
            results: function(data) {
                alert(JSON.stringify(data))
                return data;
            },
            cache: true
        }
    }).on('select2-open', function() {
        $(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
    });*/

}