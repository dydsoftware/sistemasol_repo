const d = document;

function calcular() {
    let lote = parseInt(d.getElementById("valorL").value) || 0,
        adelanto = parseInt(d.getElementById("adelanto").value) || 0,
        nCuota = parseInt(d.getElementById("cuotas").value) || 0,
        cambio = parseInt(d.getElementById("cambio").value) || 0;

    parseInt(d.getElementById("totalCuo").value) || 0;
    parseInt(d.getElementById("total").value) || 0;

    d.getElementById("total").value = (lote - adelanto).toFixed(2);
    d.getElementById("totalCuo").value = ((lote - adelanto) / nCuota).toFixed(2);
    d.getElementById("convPeso").value = (((lote - adelanto) / nCuota) * cambio).toFixed(2);
};

// funcion autocompletar

function search(name) {
    console.log(name);
    fetchSearchData(name);
}

function fetchSearchData(name) {
    fetch('../admin/phpfiles/buscar.php', {
            method: 'POST',
            body: new URLSearchParams('nya=' + name)
        })
        .then(res => res.json())
        .then(res => viewSearchResoult(res))
        .catch(e => console.error('Error: ' + e))
}

function viewSearchResoult(data) {
    const dataViewer = document.getElementById('dataViewer');

    dataViewer.innerHTML = '';

    for (let i = 0; i < data.length; i++) {
        const li = document.createElement('li');
        li.innerHTML = data[i]['nya'];
        dataViewer.appendChild(li);
    }
}

function listar_clientes() {
    var dataTable = $('#tabla-clientes').DataTable({
        "columnDefs": [
            { "width": "5%", "targets": 0 },
            { "width": "40%", "targets": 1 },
            { "width": "40%", "targets": 2 }
        ],
        "processing": true,
        paging: false,
        bSortable: true,
        "bInfo": false,
        "bLengthChange": false,
        aaSorting: [
            [1, 'asc']
        ],

        pageLength: 100,

        "serverSide": true,
        "language": {
            "sSearch": "Buscar: ",
            "processing": "Procesando...",
            "paginate": {
                "first": "Primera",
                "last": "Ultima",
                "next": "Siguiente",
                "previous": "Anterior"
            },

            "sZeroRecords": "No se encontraron resultados"
        },
        "ajax": {
            url: "../admin/phpfiles/listar.clientes.php", // json datasource
            type: "post", // method  , by default get
            error: function() { // error handling
                $(".employee-grid-error").html("");
                $("#tabla-clientes").html('<tbody class="tabla-clientes-error"><tr><th colspan="3">No se encontraron resultados</th></tr></tbody>');
                $("#tabla-clientes_processing").css("display", "none");

            }
        }
    });
}

/*Listar Vendedores*/
function listar_vendedores() {
    $('#tabla-vendedores').DataTable().destroy()
    var table = $('#tabla-vendedores').DataTable({
        "dom": "<'row'<'col-sm-12 col-md-6'B><'col-sm-12 col-md-6'f>><'row'<'col-sm-12't>><'row'<'col-sm-12 col-md-5'l><'col-sm-12 col-md-7'p>>",
        "processing": true,
        "serverSide": true,
        "ordering": false,
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
        },
        "ajax": {
            'url': "../admin/phpfiles/listar.vendedores.php",
            'type': "post", // method  , by default get
            complete: function() {
                console.log("exitoso");
            },
            error: function() { // error handling
                $(".employee-grid-error").html("");
                $("#tabla-vendedores").html('<tbody class="tabla-vendedores-error"><tr><th colspan="3">No se encontraron resultados</th></tr></tbody>');
                $("#tabla-vendedores_processing").css("display", "none");

            }
        }
    });
}

/*disparar mensaje small*/
function fireSuccess(title) {
    Swal.mixin({
        toast: true,
        position: 'top',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: false,
        onOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    }).fire({
        icon: 'success',
        title: title
    });
}


/*listar lotes listado.lote.php*/
function listar_lotes() {

    $('#datatable').DataTable().destroy()
    var table = $('#datatable').DataTable({
        "dom": "<'row'<'col-sm-12 col-md-6'B><'col-sm-12 col-md-6'f>><'row'<'col-sm-12't>><'row'<'col-sm-12 col-md-5'l><'col-sm-12 col-md-7'p>>",
        "processing": true,
        "serverSide": true,
        "ordering": false,
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
        },
        "ajax": {
            'url': "../admin/phpfiles/listar.lotes.php",
            'type': "post", // method  , by default get
            complete: function() {
                console.log("exitoso");
            },

        }
    });

}

/*Eliminar lote - listado.lote.php*/
function eliminar_lote(lote_id) {

    Swal.mixin({
        toast: true,
        position: 'top',
        showConfirmButton: true,
        showCancelButton: true,
        timerProgressBar: false,
        customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
    }).fire({
        icon: 'warning',
        title: '¿Seguro que quiere eliminar?',
        confirmButtonText: 'Si, eliminar',
        cancelButtonText: 'Cancelar!'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: 'post',
                data: { 'id': lote_id },
                url: "../admin/phpfiles/eliminar.lote.php",
                success: function(data) {
                    $('#datatable').DataTable().ajax.reload();
                    fireSuccess('Eliminado exitoso');
                }
            });
        }

    })

}





function listar_clientes_select() {
    $("#lista-clientes").select2({
        /* language: {
             "url": "https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/i18n/es.js"
         },*/
        tags: true,
        placeholder: 'Buscar un cliente',
        minimumInputLength: 2,
        allowClear: true,
        ajax: {
            url: "../admin/phpfiles/listar.clientes.select.php",
            dataType: 'json',
            data: function(term) {
                return {
                    cliente: term, // search term
                };
            },
            results: function(data) {
                alert(JSON.stringify(data))
                return data;
            },
            cache: true
        }
    });

}

